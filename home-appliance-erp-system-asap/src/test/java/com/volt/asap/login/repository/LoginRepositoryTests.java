package com.volt.asap.login.repository;

import com.volt.asap.config.HomeApplianceErpSystemAsapApplication;
import com.volt.asap.hrm.entity.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        HomeApplianceErpSystemAsapApplication.class
})
public class LoginRepositoryTests {
    @Autowired
    private LoginRepository loginRepository;

    @Test
    public void 모든_사원_가져오기_테스트() {
        List<Employee> employeeList = loginRepository.findAll();

        assertFalse(employeeList.isEmpty());
    }
}
