package com.volt.asap.groupware.repository;

import com.volt.asap.config.HomeApplianceErpSystemAsapApplication;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@ContextConfiguration(classes = {
        HomeApplianceErpSystemAsapApplication.class
})
public class DraftFormRepositoryTest {
    @Autowired
    private DraftFormRepository draftFormRepository;

    @Test
    @DisplayName("기안서 양식 구분 가져오기 테스트")
    public void findDraftFormTypeAllNative() {
        List<String> data = draftFormRepository.findDraftFormTypeAllNative();

        System.out.println("data = " + data);

        assertFalse(data.isEmpty());
    }
}