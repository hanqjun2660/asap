document.addEventListener('DOMContentLoaded', () => {
    let calendarElem = document.querySelector('div#fullCalendar');
    let calendar = new FullCalendar.Calendar(calendarElem, {
        initialView: 'dayGridMonth',
         eventSources: [{
            url: '/groupware/schedule/getSchedule',
            method: 'POST'
        }]
    });

    calendar.render();
});