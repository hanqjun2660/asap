window.onload = function () {
    tinymce.init({
        selector: 'div#webEditor',
        width: '100%',
        height: '600px',
        skin: 'oxide',
        plugins: [
            'advlist', 'autolink', 'link', 'image', 'lists', 'charmap', 'preview', 'anchor', 'pagebreak',
            'searchreplace', 'wordcount', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'insertdatetime',
            'media', 'table', 'emoticons', 'template', 'help'
        ],
        toolbar: 'undo redo | styles | bold italic | alignleft aligncenter alignright alignjustify | ' +
            'bullist numlist outdent indent | link image | print preview media fullscreen | ' +
            'forecolor backcolor emoticons | help',
        menubar: 'file edit view insert format tools table help'
    });

    /* 등록 버튼 누르면 발생하는 이벤트 할당 - click */
    let submitButton = document.querySelector('#content #writeBtn');
    submitButton.addEventListener('click', () => {
        if(isValidWriteContent()) {
            doDraftFormWrite();
        }
    });

    updateDataList();
}

 /**
  *  수정할 데이터들의 유효성을 체크한다. 제목과 구분을 아예 입력하지 않는경우 실패한다.
  * @returns {boolean} 유효성 체크 성공시 true, 실패시 false
  */
 function isValidWriteContent() {

    let titleElement = document.querySelector('.draft-form-info #draftFormTitle');
    if(titleElement.value === "") {
        alert('제목을 입력해주세요.');
        titleElement.focus();
        return false;
    }

    let typeElement = document.querySelector('.draft-form-info #draftFormType');
    if(typeElement.value === "") {
        alert('구분을 입력해주세요.');
        typeElement.focus();
        return false;
    }

    return true;
}

 /**
  * AJAX를 사용해 기안서 양식을 등록하는 함수
  */
 function doDraftFormWrite() {
    $.ajax({
        type: 'post',
        url: '/groupware/draftForm/write',
        traditional: true,
        contentType: false,
        processData: false,
        data: getData(),
        success: function (data) {
            if(data.message) {
                alert(data.message);
                location.href = '/groupware/draftForm';
            }
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('기안서 양식의 등록에 실패했습니다.');
            }
        }
    });
}

function doTempSave() {

}

 /**
  * AJAX에 포함될 데이터를 FormData로 패키징해서 반환하는 함수
  * @returns {FormData} 등록될 기안서 양식 정보가 담긴 폼데이터
  */
 function getData() {
    let formData = new FormData();

    formData.append('title', document.querySelector('.draft-form-info input#draftFormTitle').value);
    formData.append('type', document.querySelector('.draft-form-info input#draftFormType').value);
    formData.append('content', tinymce.get('webEditor').getContent());

    return formData;
}

 /**
  * 자동완성을 위한 기안서 양식 구분 리스트를 AJAX 방식으로 가져오고, 자동완성 함수를 호출하는 함수
  */
 function updateDataList() {
     $.ajax({
         type: 'get',
         url: '/groupware/draftForm/getDraftFormType',
         success: function (data) {
             autocomplete(document.querySelector('#content input#draftFormType'), data);
         },
         error: function (error) {
             if (error.message) {
                 alert(error.message)
             } else {
                 alert('기안서 양식 구분을 로드하는데 실패했습니다.');
             }
         }
     });
 }
