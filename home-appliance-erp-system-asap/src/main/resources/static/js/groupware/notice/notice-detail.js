window.onload = function () {
    let noticeNo = document.location.pathname.split('/')[2];

    let deleteBtn = document.querySelector('#content .schedule-navigation button#deleteBtn');
    deleteBtn.addEventListener('click', () => {
        if(!isNaN(parseInt(noticeNo))) {
            $.ajax({
                type: 'post',
                url: '/notice/' + noticeNo + '/remove',
                success: function (data) {
                    alert(data.message);
                    location.href = '/notice/list/';
                },
                error: function (error) {
                    if(error.message) {
                        alert(error.message);
                    } else {
                        alert('알수없는 이유로 일정 삭제에 실패했습니다.');
                    }
                }
            });
        }
    });
}