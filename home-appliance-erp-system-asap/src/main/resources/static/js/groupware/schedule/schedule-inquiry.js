window.onload = function () {
    setTimeout(() => {
        if(message) {
            alert(message);
        }
    }, 500);

    document.querySelector('#content form').addEventListener('submit', checkSubmit);

    addEventToTable();
}

/*
 * 검색 버튼을 누르면 검색어 입력에 대해 유효성 체크하는 함수
 * @param event 발생한 이벤트 객체 SubmitEvent
 */
function checkSubmit(event) {
    event.preventDefault();

    if(event.target instanceof HTMLFormElement) {
        if(event.target.querySelector('#endDate').value !== '' ^ event.target.querySelector('#startDate').value !== '') {
            alert('날짜로 검색하려면 시작과 종료 모두 입력해 주세요.');
        } else {
            event.target.submit();
        }
    }
}

/**
 * 테이블의 행들을 클릭했을 때 해당하는 상세보기 페이지로 이동하는 이벤트를 등록하는 함수
 */
function addEventToTable() {
    const elements = document.querySelectorAll('.result .table-list tbody tr');

    elements.forEach((element) => {
        element.addEventListener('click', (event) => {
            const scheduleNo = element.querySelector('#scheduleNo').innerText;
            location.href = '/groupware/schedule/' + scheduleNo;
        });
    });
}