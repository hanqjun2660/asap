package com.volt.asap.sales.repository;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.util.Streamable;

import com.volt.asap.sales.entity.OrderAndProduct;

public interface OrderAndProductRepository extends JpaRepository<OrderAndProduct, String> {

//	List<OrderAndProduct> findByOrderDateContaining(String searchValue1, Pageable paging);

//	List<OrderAndProduct> findByProductProductNameContaining(String searchWord, Pageable paging);

//	int countByOrderDateContaining(String searchValue1);

//	int countByProductProductNameContaining(String searchWord);

	OrderAndProduct findOrderAndProductEntityByOrderNumber(int orderNumber);

	int countByOrderDateBetween(Date startDate, Date endDate);

//	List<OrderAndProduct> findByOrderDateBetween(Date startDate, Date endDate, Pageable paging);
//
//	List<OrderAndProduct> findByOrderDateBetweenAndProductProductNameContaining(Date startDate, Date endDate,
//			String searchWord, Pageable paging);

	int countByOrderDateBetweenAndProductProductNameContaining(Date startDate, Date endDate, String searchWord);


	List<OrderAndProduct> findAllByOrderDateBetweenAndProductProductNameContaining(Date startDate, Date endDate,
			String searchWord, Pageable paging);

	List<OrderAndProduct> findAllByOrderDateBetween(Date startDate, Date endDate, Pageable paging);

	List<OrderAndProduct> findAllByProductProductName(String searchWord, Pageable paging);

	int countByProductProductNameContaining(String searchWord);

	List<OrderAndProduct> findAllByProductProductNameContaining(String searchWord, Pageable paging);









}
