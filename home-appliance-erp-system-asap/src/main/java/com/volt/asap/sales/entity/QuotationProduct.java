package com.volt.asap.sales.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.volt.asap.logistic.entity.Product;
import com.volt.asap.sales.dto.QuotationProductPK;

@Entity(name = "QuotationProduct")
@Table(name = "TBL_QUOTATION_PRODUCT")
@IdClass(QuotationProductPK.class)
public class QuotationProduct implements Serializable {
	private static final long serialVersionUID = 3118657244024513219L;

	@Id
	@Column(name = "ESTIMATE_CODE")
	private int estimateCode;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "PRODUCT_CODE")
	private Product product;
	
	@Column(name = "ESTI_DISCOUNT")
	private int discount;
	
	@Column(name="ESTI_PRODUCT_COUNT")
	private int productCount;
	
	@Column(name = "ESTI_PRODUCT_SUMPRICE")
	private int sumPrice;

	public QuotationProduct() {
	}

	public QuotationProduct(int estimateCode, Product product, int discount, int productCount, int sumPrice) {
		this.estimateCode = estimateCode;
		this.product = product;
		this.discount = discount;
		this.productCount = productCount;
		this.sumPrice = sumPrice;
	}

	public int getEstimateCode() {
		return estimateCode;
	}

	public void setEstimateCode(int estimateCode) {
		this.estimateCode = estimateCode;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getProductCount() {
		return productCount;
	}

	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}

	public int getSumPrice() {
		return sumPrice;
	}

	public void setSumPrice(int sumPrice) {
		this.sumPrice = sumPrice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "QuotationProduct [estimateCode=" + estimateCode + ", product=" + product + ", discount=" + discount
				+ ", productCount=" + productCount + ", sumPrice=" + sumPrice + "]";
	}
	
	
}
