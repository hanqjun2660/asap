package com.volt.asap.sales.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.volt.asap.sales.dto.POrderProductPK;
import com.volt.asap.sales.entity.POrderProduct;

public interface POrderProductRepository extends JpaRepository<POrderProduct, POrderProductPK> {

	@Query(value = "SELECT A.*, B.*\r\n"
			+ "  FROM TBL_ORDER_PRODUCT A\r\n"
			+ "  JOIN TBL_PRODUCT B ON (A.PRODUCT_CODE = B.PRODUCT_CODE)", nativeQuery = true)
	List<POrderProduct> findPOProductName();

	@Modifying
	@Transactional
	@Query(value = "INSERT INTO TBL_ORDER_PRODUCT(PRODUCT_CODE,P_ORDER_CODE,PRODUCT_COUNT,PRODUCT_SUMPRICE)\r\n"
			+ "VALUES(:productCode, SEQ_P_ORDER_CODE.currval,:count,:sumMoney)" , nativeQuery = true)
	int insertQuery(String productCode, int sumMoney, int count);


}
