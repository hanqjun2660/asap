package com.volt.asap.sales.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.CompanyInfoDTO;
import com.volt.asap.hrm.entity.CompanyInfo;
import com.volt.asap.hrm.repository.CompanyInfoRepository;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.entity.Product;
import com.volt.asap.logistic.repository.ProductRepository;
import com.volt.asap.sales.dto.OrderListDTO;
import com.volt.asap.sales.entity.Order;
import com.volt.asap.sales.entity.OrderAndProduct;
import com.volt.asap.sales.repository.OrderAndProductRepository;
import com.volt.asap.sales.repository.OrderRepository;

@Service
public class OrderService {

	private final OrderRepository orderRepository;
	private final ProductRepository productRepository;
	private final OrderAndProductRepository orderAndProductRepository;
	private final CompanyInfoRepository companyInfoRepository;
	private final ModelMapper modelMapper;

	@Autowired
	public OrderService(OrderRepository orderRepository, ProductRepository productRepository,
			OrderAndProductRepository orderAndProductRepository, ModelMapper modelMapper, CompanyInfoRepository companyInfoRepository) {
		this.orderRepository = orderRepository;
		this.productRepository = productRepository;
		this.orderAndProductRepository = orderAndProductRepository;
		this.companyInfoRepository = companyInfoRepository;
		this.modelMapper = modelMapper;
	}

//	public List<OrderListDTO> findOrderList(SelectCriteriaforDoubleValue selectCriteriaforDoubleValue) {
//		
//		int index = selectCriteriaforDoubleValue.getPageNo() - 1;			// Pageble객체를 사용시 페이지는 0부터 시작(1페이지가 0)
//		int count = selectCriteriaforDoubleValue.getLimit();
//		String searchValue1 = selectCriteriaforDoubleValue.getSearchValue1();
//		String searchValue2 = selectCriteriaforDoubleValue.getSearchValue2();
//		
//		/* 페이징 처리와 정렬을 위한 객체 생성 */
//		Pageable paging = PageRequest.of(index, count, Sort.Direction.DESC,("orderNumber"));
//		
//		List<OrderAndProduct> orderList = orderAndProductRepository.findAll(Sort.by("orderNumber"));
//		
//		if(searchValue1 != null && searchValue2 != null) {}
//		else if(searchValue1 != null && searchValue2 == null) {
//			/* 주문일자로 검색할때 */
//			orderList = orderAndProductRepository.findByOrderDateContaining(searchValue1,paging);
//		}
//		else if(searchValue1 == null && searchValue2 != null) {
//			/* 상품명로 검색할때 */
//			orderList = orderAndProductRepository.findByProductProductNameContaining(searchValue2,paging);
//		}else {
//			orderList = orderAndProductRepository.findAll(paging).toList();
//		}
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}

	public void registNewOrder(OrderListDTO newOrder) {

		orderRepository.save(modelMapper.map(newOrder, Order.class));
	}

	public OrderListDTO findOrderByNumber(int orderNumber) {

		OrderAndProduct orderdetail = orderAndProductRepository.findOrderAndProductEntityByOrderNumber(orderNumber);

		return modelMapper.map(orderdetail, OrderListDTO.class);
	}

	@Transactional
	public void modifyOrder(OrderListDTO orderUpdate) {

		Order updateOrder = orderRepository.findOrderEntityByOrderNumber(orderUpdate.getOrderNumber());

		updateOrder.setOrderDate(orderUpdate.getOrderDate());
		updateOrder.setOrderPrice(orderUpdate.getOrderPrice());
		updateOrder.setOrderShippingNumber(orderUpdate.getOrderShippingNumber());
		updateOrder.setProductCode(orderUpdate.getProductCode());
		updateOrder.setOrderShippingStatus(orderUpdate.getOrderShippingStatus());

	}

	public List<ProductDTO> productList() {
		List<Product> productList = productRepository.findAll();
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class))
				.collect(Collectors.toList());
	}

//	public int selectTotalCount(String searchCondition1, String searchCondition2, String searchValue1,
//			String searchValue2) {
//
//		int count = 0;
//		
//		if(searchValue1 != null && searchValue2 != null) {
//			if(!searchValue1.isBlank() && !searchValue2.isEmpty()) {
//				System.out.println("둘다 검색");
//			}else if(!searchValue1.isEmpty() && searchValue2.isEmpty()) {
//				count = orderAndProductRepository.countByOrderDateContaining(searchValue1);
//				System.out.println("count" + count);
//			}else if(searchValue1.isEmpty() && !searchValue2.isEmpty()) {
//				count = orderAndProductRepository.countByProductProductNameContaining(searchValue2);
//				System.out.println("count" + count);
//			} else {
//				System.out.println("아무것도 없이 검색 버튼을 눌렀을 때");
//				count = (int)orderAndProductRepository.count();
//				System.out.println("count" + count);
//			}
//		} else {
//			System.out.println("노 검색");
//			count = (int)orderAndProductRepository.count();
//			System.out.println("count" + count);
//		}
//
//		return count;
//	}

	public List<OrderAndProduct> productNameList() {

		List<Product> productNameList = productRepository.findAll();
		return productNameList.stream().map(product -> modelMapper.map(product, OrderAndProduct.class))
				.collect(Collectors.toList());
	}

//	public int selectTotalCount() {
//		int count = 0;
//		count = (int) orderAndProductRepository.count();
//		return count;
//	}

//	public int selectTotalCount1(Date startDate, Date endDate, String searchWord) {
//		int count = 0;
//
//		if (startDate != null && endDate != null && !isEmptyString(searchWord)) {
//		} else if (startDate != null && endDate != null && isEmptyString(searchWord)) {
//			count = orderAndProductRepository.countByOrderDateBetween(startDate, endDate);
//		} else if (startDate == null && endDate == null && !isEmptyString(searchWord)) {
//			count = orderAndProductRepository.countByProductProductNameContaining(searchWord);
//		} else {
//			count = (int) orderAndProductRepository.count();
//		}
//
//		return count;
//	}

//	public List<OrderListDTO> orderList(SelectCriteriaforDate selectCriteriaforDate) {
//		int index = selectCriteriaforDate.getPageNo() - 1;
//		int count = selectCriteriaforDate.getLimit();
//		Date startDate = selectCriteriaforDate.getStartDate();
//		Date endDate = selectCriteriaforDate.getEndDate();
//		String searchWord = selectCriteriaforDate.getSearchWord();
//
//		Pageable paging = PageRequest.of(index, count);
//
//		System.out.println("찾는동로?" + searchWord);
//		List<OrderAndProduct> orderList = orderAndProductRepository.findAll(Sort.by("orderNumber").descending());
//
//		if (startDate != null && endDate != null && !isEmptyString(searchWord)) {
////			orderList = orderAndProductRepository.findByOrderDateBetween(startDate, endDate, paging);
//			orderList = orderAndProductRepository.findByOrderDateBetweenAndProductProductNameContaining(startDate,
//					endDate, searchWord, paging);
//		} else if (startDate != null && endDate != null && isEmptyString(searchWord)) {
////			orderList = orderAndProductRepository.findByOrderDateBetweenAndProductProductNameContaining(startDate, endDate,searchWord, paging);
//			orderList = orderAndProductRepository.findByOrderDateBetween(startDate, endDate, paging);
//		} else if (startDate == null && endDate == null && !isEmptyString(searchWord)) {
//			orderList = orderAndProductRepository.findByProductProductNameContaining(searchWord, paging);
//			System.out.println("단어야 나오니?" + searchWord);
//		} else {
//			orderList = orderAndProductRepository.findAll(paging).toList();
//		}
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}

	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다. */
		return str == null || "".equals(str);
	}

	private Sort sortByDate() {
		return Sort.by(Sort.Direction.ASC, "orderDate");
	}

	public int allCount() {
		int count = 0;

		count = (int) orderAndProductRepository.count();

		return count;
	}

	public int dateTotalCount(Date sDate, Date eDate) {
		int count = 0;

		count = orderAndProductRepository.countByOrderDateBetween(sDate, eDate);

		return count;
	}

	public int orderProductCount(Date sDate, Date eDate, String proName) {
		int count = 0;

		count = orderAndProductRepository.countByOrderDateBetweenAndProductProductNameContaining(sDate, eDate, proName);

		return count;
	}

//	public List<OrderListDTO> findAllList() {
//		List<OrderAndProduct> orderList = orderAndProductRepository.findAll(paging).toList();
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}

//	public List<OrderListDTO> findOrderList(Date sDate, Date eDate, String proName) {
//		List<OrderAndProduct> orderList = orderAndProductRepository
//				.findAllByOrderDateBetweenAndProductProductNameContaining(sDate, eDate, proName);
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}
//
//	public List<OrderListDTO> findOrderListByDate(Date sDate, Date eDate) {
//		List<OrderAndProduct> orderList = orderAndProductRepository.findAllByOrderDateBetween(sDate, eDate);
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}
//
//	public List<OrderListDTO> findOrderListByProductName(String proName) {
//		List<OrderAndProduct> orderList = orderAndProductRepository.findAllByProductProductName(proName);
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}

//----------------
	public int countOrderByQueries(Map<String, String> parameterMap) {
		if (parameterMap == null) {
			/* 전체 조회 */

			return (int) orderAndProductRepository.count();
		} else {
			/* 검색어 입력을 통한 부분 조회 */
			Date startDate = toSqlDate(parameterMap.get("startDate"));
			Date endDate = toSqlDate(parameterMap.get("endDate"));
			String searchWord = parameterMap.get("searchWord");

			if (startDate != null && endDate != null) {
				if ("".equals(searchWord)) {
					/* 날짜 검색 */
					return orderAndProductRepository.countByOrderDateBetween(startDate, endDate);
							
				} else {
					/* 날짜, 검색어 */
					return orderAndProductRepository.countByOrderDateBetweenAndProductProductNameContaining(startDate, endDate, searchWord);
				}
			} else {
				/* 검색어 */
				return orderAndProductRepository.countByProductProductNameContaining(searchWord);
			}
		}
	}

	public List<OrderListDTO> findOrderByQueries(CustomSelectCriteria selectCriteria) {
		/* Pageable 객체는 1페이지를 0번으로 인식 */
        int index = selectCriteria.getPageNo() - 1;
        int count = selectCriteria.getLimit();

        Pageable paging = PageRequest.of(index, count, Sort.Direction.DESC,("orderNumber"));
        
		List<OrderAndProduct> orderList = null;

		if(selectCriteria.getSearchValueMap() == null) {
            /* 전체 조회 */
			
			orderList = orderAndProductRepository.findAll(paging).toList();
        } else {
            /* 검색어 입력을 통한 부분 조회 */
            java.sql.Date startDate = toSqlDate(selectCriteria.getSearchValueMap().get("startDate"));
            java.sql.Date endDate = toSqlDate(selectCriteria.getSearchValueMap().get("endDate"));
            String searchWord = selectCriteria.getSearchValueMap().get("searchWord");
            
            if(startDate != null && endDate != null) {
                if("".equals(searchWord)) {
                    /* 날짜 검색 */
                	orderList =orderAndProductRepository.findAllByOrderDateBetween(startDate, endDate,paging);
                } else {
                    /* 날짜, 검색어 */
                	orderList = orderAndProductRepository.findAllByOrderDateBetweenAndProductProductNameContaining(startDate, endDate, searchWord,paging);
                }
            } else {
                /* 검색어 */orderList = orderAndProductRepository.findAllByProductProductNameContaining(searchWord,paging);
            }
        }
		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
	}
//	public List<OrderListDTO> findAllList() {
//		List<OrderAndProduct> orderList = orderAndProductRepository.findAll();
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}
//
//	public List<OrderListDTO> findOrderList(Date sDate, Date eDate, String proName) {
//		List<OrderAndProduct> orderList = orderAndProductRepository
//				.findAllByOrderDateBetweenAndProductProductNameContaining(sDate, eDate, proName);
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}
//
//	public List<OrderListDTO> findOrderListByDate(Date sDate, Date eDate) {
//		List<OrderAndProduct> orderList = orderAndProductRepository.findAllByOrderDateBetween(sDate, eDate);
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}
//
//	public List<OrderListDTO> findOrderListByProductName(String proName) {
//		List<OrderAndProduct> orderList = orderAndProductRepository.findAllByProductProductName(proName);
//		return orderList.stream().map(order -> modelMapper.map(order, OrderListDTO.class)).collect(Collectors.toList());
//	}
	private java.sql.Date toSqlDate(String dateString) {
		java.sql.Date endDate = null;

		if (dateString != null && !"".equals(dateString)) {
			endDate = java.sql.Date.valueOf(dateString);
		}

		return endDate;
	}


	public CompanyInfoDTO findCompanyInfo(String companyName) {
		
		CompanyInfo companyInfo = companyInfoRepository.findCompanyInfoByCompanyName(companyName);
		
		return modelMapper.map(companyInfo, CompanyInfoDTO.class);
	}
}
