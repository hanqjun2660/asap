package com.volt.asap.sales.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "Client")
@Table(name = "TBL_CLIENT")
@SequenceGenerator(
		name = "CLIENT_SEQ_CLIENT_NO",
		sequenceName = "SEQ_CLIENT_NO",
		initialValue = 1,
		allocationSize = 1
)
public class Client {

	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "CLIENT_SEQ_CLIENT_NO"
	)
	@Column(name = "CLIENT_NO")
	private int no;
	
	@Column(name = "CLIENT_NAME")
	private String name;
	
	@Column(name = "CLIENT_REPRESENTATIVE_NAME")
	private String representativeName;
	
	@Column(name = "CLIENT_PHONE")
	private String phone;
	
	@Column(name = "CLIENT_TELEPHONE")
	private String telephone;
	
	@Column(name = "CLIENT_EMAIL")
	private String email;
	
	@Column(name = "CLIENT_ADDRESS")
	private String address;
	
	@Column(name = "CLIENT_MAINTAIN")
	private String maintain;

	public Client() {
	}

	public Client(int no, String name, String representativeName, String phone, String telephone, String email,
			String address, String maintain) {
		this.no = no;
		this.name = name;
		this.representativeName = representativeName;
		this.phone = phone;
		this.telephone = telephone;
		this.email = email;
		this.address = address;
		this.maintain = maintain;
	}
	
	public void setClient(int no, String name, String representativeName, String phone, String telephone, String email,
			String address, String maintain) {
		this.no = no;
		this.name = name;
		this.representativeName = representativeName;
		this.phone = phone;
		this.telephone = telephone;
		this.email = email;
		this.address = address;
		this.maintain = maintain;
	}
	
	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRepresentativeName() {
		return representativeName;
	}

	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMaintain() {
		return maintain;
	}

	public void setMaintain(String maintain) {
		this.maintain = maintain;
	}

	@Override
	public String toString() {
		return "Client [no=" + no + ", name=" + name + ", representativeName=" + representativeName + ", phone=" + phone
				+ ", telephone=" + telephone + ", email=" + email + ", address=" + address + ", maintain=" + maintain
				+ "]";
	}
	
	
	
	
}
