package com.volt.asap.sales.dto;

import java.sql.Date;
import java.util.List;

import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.sales.entity.QuotationProduct;

public class EstimateDTO {

	private int code;
	private Date date;
	private String client; 
	private Date expiryDate;
	private int sumMoney;
	private String progress;
	private Employee emp;
	private String status;
	private List<QuotationProduct> productList;
	public EstimateDTO() {
	}
	public EstimateDTO(int code, Date date, String client, Date expiryDate, int sumMoney, String progress, Employee emp,
			String status, List<QuotationProduct> productList) {
		this.code = code;
		this.date = date;
		this.client = client;
		this.expiryDate = expiryDate;
		this.sumMoney = sumMoney;
		this.progress = progress;
		this.emp = emp;
		this.status = status;
		this.productList = productList;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public int getSumMoney() {
		return sumMoney;
	}
	public void setSumMoney(int sumMoney) {
		this.sumMoney = sumMoney;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public Employee getEmp() {
		return emp;
	}
	public void setEmp(Employee emp) {
		this.emp = emp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<QuotationProduct> getProductList() {
		return productList;
	}
	public void setProductList(List<QuotationProduct> productList) {
		this.productList = productList;
	}
	@Override
	public String toString() {
		return "EstimateDTO [code=" + code + ", date=" + date + ", client=" + client + ", expiryDate=" + expiryDate
				+ ", sumMoney=" + sumMoney + ", progress=" + progress + ", emp=" + emp + ", status=" + status
				+ ", productList=" + productList + "]";
	}
	
	
	
	
		
}
