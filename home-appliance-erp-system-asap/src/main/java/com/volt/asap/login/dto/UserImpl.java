package com.volt.asap.login.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.sql.Date;
import java.util.Collection;

/**
 * User를 상속받은 UserImpl
 * UserDetail의 기본적인 기능에 추가적인 정보를 저장하기 위한 객체
 *
 * @see User
 */
public class UserImpl extends User {
    private static final long serialVersionUID = 6313962668052927304L;

    private int empCode;                    // EMP_CODE
    private String empName;                 // EMP_NAME
    private String empBirth;                // EMP_BIRTH
    private String deptCode;                // DEPT_CODE
    private String empPhone;                // EMP_PHONE
    private String empEmail;                // EMP_EMAIL
    private String empAddress;              // EMP_ADDRESS
    private java.sql.Date empJoindate;      // EMP_JOINDATE
    private java.sql.Date empLeavedate;     // EMP_LEAVEDATE
    private String empLeaveYn;              // EMP_LEAVE_YN
    private String jobCode;                 // JOB_CODE
    private int annualLeftDay;              // ANNUAL_LEFT_DAY
    private String signImg;                 // SIGN_IMG
    private String profileImg;              // PROFILE_IMG
    private String empGender;               // EMP_GENDER
    private String empPwd;                  // EMP_PWD
    private int salaryBase;                 // SALARY_BASE

    /**
     * 상위 클래스인 {@link User}의 생성자를 구현
     *
     * @param username 계정 로그인에 필요한 ID
     * @param password 계정 로그인에 필요한 Password
     * @param authorities 해당 사용자가 지닌 권한들
     */
    public UserImpl(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    /**
     * {@link EmployeeDTO} 정보로 각종 필드에 데이터를 초기화하는 메서드
     *
     * @param employee Employee 정보가 담겨있는 객체
     */
    public void setDetails(EmployeeDTO employee) {
        this.empCode = employee.getEmpCode();
        this.empName = employee.getEmpName();
        this.empBirth = employee.getEmpBirth();
        this.deptCode = employee.getDeptCode();
        this.empPhone = employee.getEmpPhone();
        this.empEmail = employee.getEmpEmail();
        this.empAddress = employee.getEmpAddress();
        this.empJoindate = employee.getEmpJoindate();
        this.empLeavedate = employee.getEmpLeavedate();
        this.empLeaveYn = employee.getEmpLeaveYn();
        this.jobCode = employee.getJobCode();
        this.annualLeftDay = employee.getAnnualLeftDay();
        this.signImg = employee.getSignImg();
        this.profileImg = employee.getProfileImg();
        this.empGender = employee.getEmpGender();
        this.empPwd = employee.getEmpPwd();
        this.salaryBase = employee.getSalaryBase();
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpBirth() {
        return empBirth;
    }

    public void setEmpBirth(String empBirth) {
        this.empBirth = empBirth;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getEmpPhone() {
        return empPhone;
    }

    public void setEmpPhone(String empPhone) {
        this.empPhone = empPhone;
    }

    public String getEmpEmail() {
        return empEmail;
    }

    public void setEmpEmail(String empEmail) {
        this.empEmail = empEmail;
    }

    public String getEmpAddress() {
        return empAddress;
    }

    public void setEmpAddress(String empAddress) {
        this.empAddress = empAddress;
    }

    public Date getEmpJoindate() {
        return empJoindate;
    }

    public void setEmpJoindate(Date empJoindate) {
        this.empJoindate = empJoindate;
    }

    public Date getEmpLeavedate() {
        return empLeavedate;
    }

    public void setEmpLeavedate(Date empLeavedate) {
        this.empLeavedate = empLeavedate;
    }

    public String getEmpLeaveYn() {
        return empLeaveYn;
    }

    public void setEmpLeaveYn(String empLeaveYn) {
        this.empLeaveYn = empLeaveYn;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public int getAnnualLeftDay() {
        return annualLeftDay;
    }

    public void setAnnualLeftDay(int annualLeftDay) {
        this.annualLeftDay = annualLeftDay;
    }

    public String getSignImg() {
        return signImg;
    }

    public void setSignImg(String signImg) {
        this.signImg = signImg;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getEmpGender() {
        return empGender;
    }

    public void setEmpGender(String empGender) {
        this.empGender = empGender;
    }

    public String getEmpPwd() {
        return empPwd;
    }

    public void setEmpPwd(String empPwd) {
        this.empPwd = empPwd;
    }

    public int getSalaryBase() {
        return salaryBase;
    }

    public void setSalaryBase(int salaryBase) {
        this.salaryBase = salaryBase;
    }

    @Override
    public String toString() {
        return "UserImpl{" +
                "empCode=" + empCode +
                ", empName='" + empName + '\'' +
                ", empBirth='" + empBirth + '\'' +
                ", deptCode='" + deptCode + '\'' +
                ", empPhone='" + empPhone + '\'' +
                ", empEmail='" + empEmail + '\'' +
                ", empAddress='" + empAddress + '\'' +
                ", empJoindate=" + empJoindate +
                ", empLeavedate=" + empLeavedate +
                ", empLeaveYn='" + empLeaveYn + '\'' +
                ", jobCode='" + jobCode + '\'' +
                ", annualLeftDay=" + annualLeftDay +
                ", signImg='" + signImg + '\'' +
                ", profileImg='" + profileImg + '\'' +
                ", empGender='" + empGender + '\'' +
                ", empPwd='" + empPwd + '\'' +
                ", salaryBase=" + salaryBase +
                '}';
    }
}
