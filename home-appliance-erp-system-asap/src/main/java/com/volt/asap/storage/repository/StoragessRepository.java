package com.volt.asap.storage.repository;

import com.volt.asap.storage.entity.Storage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoragessRepository extends JpaRepository<Storage, String> {


    Storage findByStorageNo(Integer number);
}
