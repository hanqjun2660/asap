package com.volt.asap.accounting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.volt.asap.hrm.entity.Employee;

@Entity(name="SalaryAndEmployee")
@Table(name="TBL_SALARY")
@SequenceGenerator(
		name = "SALARY_NO_SEQ_GENERATOR",
		sequenceName = "SEQ_SALARY_NO",
		initialValue = 65,
		allocationSize = 1
)
public class SalaryAndEmployee {

	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "SAL_SEQ_GENERATOR"
	)
	  
	@Column(name = "OVERTIME_ALLOWANCE_SUM", nullable = false)
	private int overAllowance;
	
	@Column(name = "ACTUAL_PAYMENT", nullable = false)
	private int actualPayment;
	
	@Column(name = "SALARY_NO", nullable = false)
	private int salNo;
	
	@ManyToOne
	@JoinColumn(name = "EMP_CODE", nullable = false)
	private Employee employee;
	
	@Column(name = "SALARY_MONTH", nullable = false)
	private int salaryMonth;
	
	@Column(name = "SALARY_Year", nullable = false)
	private String salaryYear;
	
	public SalaryAndEmployee() {
	}

	public SalaryAndEmployee(int overAllowance, int actualPayment, int salNo, Employee employee, int salaryMonth,
			String salaryYear) {
		super();
		this.overAllowance = overAllowance;
		this.actualPayment = actualPayment;
		this.salNo = salNo;
		this.employee = employee;
		this.salaryMonth = salaryMonth;
		this.salaryYear = salaryYear;
	}

	public int getOverAllowance() {
		return overAllowance;
	}

	public void setOverAllowance(int overAllowance) {
		this.overAllowance = overAllowance;
	}

	public int getActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(int actualPayment) {
		this.actualPayment = actualPayment;
	}

	public int getSalNo() {
		return salNo;
	}

	public void setSalNo(int salNo) {
		this.salNo = salNo;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getSalaryMonth() {
		return salaryMonth;
	}

	public void setSalaryMonth(int salaryMonth) {
		this.salaryMonth = salaryMonth;
	}

	public String getSalaryYear() {
		return salaryYear;
	}

	public void setSalaryYear(String salaryYear) {
		this.salaryYear = salaryYear;
	}

	@Override
	public String toString() {
		return "SalaryAndEmployee [overAllowance=" + overAllowance + ", actualPayment=" + actualPayment + ", salNo="
				+ salNo + ", employee=" + employee + ", salaryMonth=" + salaryMonth + ", salaryYear=" + salaryYear
				+ "]";
	}
}


