package com.volt.asap.accounting.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.volt.asap.hrm.entity.Employee;

@Entity(name="SeverancePayAndEmployee")
@Table(name="TBL_RETIRING_ALLOWANCE")
public class SeverancePayAndEmployee implements Serializable{
	private static final long serialVersionUID = 5584680226795426271L;

	@Id
	@Column(name = "EMP_CODE", insertable = false, updatable = false)
	private int empCode;
	
	@MapsId
	@OneToOne
	@JoinColumn(name = "EMP_CODE")
	private Employee employee;
	
	@Column(name = "RETIREMENT_ACTUAL_PAYMENT")
	private int retirePay;
	
	@Column(name = "RETIREMENT_STATUS")
	private String retireStatus;

	public SeverancePayAndEmployee() {
	}

	public SeverancePayAndEmployee(int empCode, Employee employee, int retirePay, String retireStatus) {
		this.empCode = empCode;
		this.employee = employee;
		this.retirePay = retirePay;
		this.retireStatus = retireStatus;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getRetirePay() {
		return retirePay;
	}

	public void setRetirePay(int retirePay) {
		this.retirePay = retirePay;
	}

	public String getRetireStatus() {
		return retireStatus;
	}

	public void setRetireStatus(String retireStatus) {
		this.retireStatus = retireStatus;
	}

	@Override
	public String toString() {
		return "SeverancePayAndEmployee [empCode=" + empCode + ", employee=" + employee + ", retirePay=" + retirePay
				+ ", retireStatus=" + retireStatus + "]";
	}

}