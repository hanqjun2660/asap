package com.volt.asap.accounting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="SeverancePay")
@Table(name="TBL_RETIRING_ALLOWANCE")
public class SeverancePay {
	
	@Id
	@Column(name = "EMP_CODE")
	private int empCode;
	
	@Column(name = "RETIREMENT_ACTUAL_PAYMENT")
	private int retirePay;
	
	@Column(name = "RETIREMENT_STATUS")
	private String retireStatus;

	public SeverancePay() {
	}

	public SeverancePay(int empCode, int retirePay, String retireStatus) {
		this.empCode = empCode;
		this.retirePay = retirePay;
		this.retireStatus = retireStatus;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public int getRetirePay() {
		return retirePay;
	}

	public void setRetirePay(int retirePay) {
		this.retirePay = retirePay;
	}

	public String getRetireStatus() {
		return retireStatus;
	}

	public void setRetireStatus(String retireStatus) {
		this.retireStatus = retireStatus;
	}

	@Override
	public String toString() {
		return "SeverancePay [empCode=" + empCode + ", retirePay=" + retirePay + ", retireStatus=" + retireStatus + "]";
	}
}
