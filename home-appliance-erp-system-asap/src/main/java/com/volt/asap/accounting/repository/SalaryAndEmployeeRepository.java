package com.volt.asap.accounting.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.accounting.dto.SalaryDTO;
import com.volt.asap.accounting.entity.Salary;
import com.volt.asap.accounting.entity.SalaryAndEmployee;

public interface SalaryAndEmployeeRepository extends JpaRepository<SalaryAndEmployee, Integer>{

	int countBySalaryMonthContaining(int salaryMonth);

	int countByEmployeeEmpNameContaining(String searchValue);

	List<SalaryAndEmployee> findBySalaryMonthContaining(int salaryMonth, Pageable paging);

	List<SalaryAndEmployee> findByEmployeeEmpNameContaining(String searchValue, Pageable paging);

	SalaryAndEmployee findBySalNoLike(int salNo);

//	SalaryAndEmployee findByEmpCode(int empCode);


}
