package com.volt.asap.accounting.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.accounting.dto.EmployeeDTO;
import com.volt.asap.accounting.dto.SeverancePayDTO;
import com.volt.asap.accounting.service.SalaryService;
import com.volt.asap.accounting.service.SeverancePayService;
import com.volt.asap.common.paging.Pagenation;
import com.volt.asap.common.paging.SelectCriteria;

@Controller
@RequestMapping("/severancePay")
public class SeverancePayController {
	
	private final SeverancePayService severancePayService;
	
	@Autowired
	public SeverancePayController(SalaryService salaryService, SeverancePayService severancePayService) {
		this.severancePayService = severancePayService;
	}

	/* 퇴직금 조회 */
	@GetMapping("/list")
	public ModelAndView selectSeverancePayList(HttpServletRequest request, ModelAndView mv) {
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		
		int totalCount = severancePayService.selectTotalCount(searchCondition, searchValue);
		
		int limit = 10;
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		if(searchValue != null && !"".equals(searchValue)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}
		
		List<SeverancePayDTO> sevList = severancePayService.findSeverancePayList(selectCriteria);
		
		mv.addObject("sevList", sevList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("severancePay/list");
		
		return mv;
	}
	

	
	@GetMapping("/regist")
	public void registSevPay() {}
	
	/* ajax 통해서 사원명 조회 */
	@GetMapping(value = "/empName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<EmployeeDTO> findEmpNameList(){
		return severancePayService.findEmployeeList();
	}
	
	@PostMapping("/regist")
	public ModelAndView registSevPay(ModelAndView mv, SeverancePayDTO severancePay, RedirectAttributes rttr) {
	
		System.out.println("입력값 확인: " + severancePay);
		severancePayService.registSevPay(severancePay);
		
		rttr.addFlashAttribute("registSuccessMessage", "퇴직금 내역이 등록되었습니다.");
		mv.setViewName("redirect:/severancePay/list");
		
		return mv;
		
	}
	
	/* 퇴직금 내역 수정 */
	@GetMapping("/modify/{empCode}")
	public ModelAndView findSevPayByCode(ModelAndView mv, @PathVariable int empCode) {
		System.out.println("컨트롤러 도착: " + empCode);
		SeverancePayDTO severancePay = severancePayService.findSevPayByCode(empCode);
		
		mv.addObject("severancePay", severancePay);
		mv.setViewName("severancePay/modify");
		
		return mv;
	}
	
	@PostMapping("modify")
	public String modifyPage(RedirectAttributes rttr, @ModelAttribute SeverancePayDTO severancePay) {
		System.out.println("퇴직금 수정 입력 내용: " + severancePay);
		severancePayService.modifySevPay(severancePay);
		
		rttr.addFlashAttribute("registSuccessMessage", "변경되었습니다.");
		
		return "redirect:/severancePay/list";
	}
	
	public boolean isEmptyString(String str) {
		return str == null || "".equals(str);
	}
	
	
	
	/* 퇴직금 내역 삭제 */
	
	@GetMapping("/delete/{empCode}")
	public ModelAndView deleteSevPay(RedirectAttributes rttr, ModelAndView mv, @PathVariable int empCode) {
		
		
		System.out.println("퇴직금 삭제 사원코드 넘어왔나^^^^^^^^^^^^^^^^^^" + empCode);
		int result = severancePayService.deleteSevPay(empCode);
		
		rttr.addFlashAttribute("registSuccessMessage", "삭제되었습니다.");

		mv.setViewName("redirect:/severancePay/list");
		
		return mv;
		
		
	}
		
}
