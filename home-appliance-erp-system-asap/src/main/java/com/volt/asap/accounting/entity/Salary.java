package com.volt.asap.accounting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;

/**
 * @author hi
 *
 */
@Entity(name="Salary")
@Table(name="TBL_SALARY")
@SequenceGenerator(
		name = "SALARY_NO_SEQ_GENERATOR",
		sequenceName = "SEQ_SALARY_NO",
		initialValue = 65,
		allocationSize = 1
)

public class Salary {
	
	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "SALARY_NO_SEQ_GENERATOR"
	)
	@Column(name = "SALARY_NO")
	private int salNo;
	
	@Column(name = "OVERTIME_ALLOWANCE_SUM")
	private int overAllowance;
	
	@Column(name = "ACTUAL_PAYMENT")
	private int actualPayment;
	
	@Column(name = "EMP_CODE")
	private  int empCode;
	
	@Column(name = "SALARY_MONTH")
	private int salaryMonth;
	
	@Column(name = "SALARY_YEAR")
	private String salaryYear;

	public Salary() {
	}

	public Salary(int overAllowance, int actualPayment, int salNo, int empCode, int salaryMonth, String salaryYear) {
		super();
		this.overAllowance = overAllowance;
		this.actualPayment = actualPayment;
		this.salNo = salNo;
		this.empCode = empCode;
		this.salaryMonth = salaryMonth;
		this.salaryYear = salaryYear;
	}

	public int getOverAllowance() {
		return overAllowance;
	}

	public void setOverAllowance(int overAllowance) {
		this.overAllowance = overAllowance;
	}

	public int getActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(int actualPayment) {
		this.actualPayment = actualPayment;
	}

	public int getSalNo() {
		return salNo;
	}

	public void setSalNo(int salNo) {
		this.salNo = salNo;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public int getSalaryMonth() {
		return salaryMonth;
	}

	public void setSalaryMonth(int salaryMonth) {
		this.salaryMonth = salaryMonth;
	}

	public String getSalaryYear() {
		return salaryYear;
	}

	public void setSalaryYear(String salaryYear) {
		this.salaryYear = salaryYear;
	}

	@Override
	public String toString() {
		return "Salary [overAllowance=" + overAllowance + ", actualPayment=" + actualPayment + ", salNo=" + salNo
				+ ", empCode=" + empCode + ", salaryMonth=" + salaryMonth + ", salaryYear=" + salaryYear + "]";
	}

}
