package com.volt.asap.accounting.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.accounting.dto.EmployeeDTO;
import com.volt.asap.accounting.dto.SalaryDTO;
import com.volt.asap.accounting.service.SalaryService;
import com.volt.asap.common.paging.Pagenation;
import com.volt.asap.common.paging.SelectCriteria;

@Controller
@RequestMapping("/salary")
public class SalaryController {
	
	private final SalaryService salaryService;
	
	@Autowired
	public SalaryController(SalaryService salaryService) {
		this.salaryService = salaryService;
	}

	/* 급여 조회 */
	@GetMapping("/list")
	public ModelAndView selectSalaryList(HttpServletRequest request, ModelAndView mv) {
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		
		int totalCount = salaryService.selectTotalCount(searchCondition, searchValue);
		
		System.out.println("전체 게시글 수 : " + totalCount);
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		if(searchValue != null && !"".equals(searchValue)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);
		
		List<SalaryDTO> salList = salaryService.findSalaryList(selectCriteria);
//		for(SalaryDTO salary : salList) {
//			System.out.println(salary);
//			System.out.println("컨트롤러 급여 내역: " + salList);
//		}

		
		mv.addObject("salList", salList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("salary/list");
		
		return mv;
	}
	
	
	
//	/* ajax 사원명 가져오기 */
//	@GetMapping("/regist")
//	public void registSevPay() {}
//	
//	@GetMapping(value = "/empName", produces = "application/json; charset = UTF-8")
//	@ResponseBody
//	public List<EmployeeDTO> findEmpNameList(){
//		return salaryService.findEmployeeList();
//	}
	
//	/* 해당 사원 급여 내역 가져오기 */
//	@PostMapping("/registSearching/{empCode}")
//	public ModelAndView selectEmpSalaryList(HttpServletRequest request, ModelAndView mv, @PathVariable int empCode) {
//		
//		SalaryDTO salary = salaryService.findSalaryList(empCode);
//		
//		mv.addObject("salary", salary);
//		mv.setViewName("salary/regist/{empCode}");
//		return mv;
//	}
	
//	/* 사원 조회 후 급여 내역 값 불러와 급여 등록 */
//	@PostMapping("/regist/{empCode}")
//	public ModelAndView registSalary(ModelAndView mv, SalaryDTO salary, RedirectAttributes rttr, @PathVariable int empCode) {
//	
//		SalaryDTO registSalary = salaryService.registSalary(empCode);
//	
//	
//	}
	
	/* 급여 내역 수정 */
	@GetMapping("/modify/{salNo}")
	public ModelAndView findModSalaryByNo(ModelAndView mv, @PathVariable int salNo) {
		
		System.out.println("컨트롤러 도착!!!!!!!!!!!!: " + salNo);
		SalaryDTO salary = salaryService.findModSalaryByNo(salNo);
		
		int over = salary.getOverAllowance();
		int total = salary.getActualPayment();
		int month = salary.getSalaryMonth();
		
		System.out.println("컨트롤러에서 SalaryDTO에 담겨있는 값");
		System.out.println("초과수당: " + over);
		System.out.println("실지급액: " + total);
		System.out.println("지급월: " + month);
		
		mv.addObject("salary", salary);
		mv.setViewName("salary/modify");
		
		return mv;
	}
	
	@PostMapping("modify")
	public String modifyPage(RedirectAttributes rttr, @ModelAttribute SalaryDTO salary) {
		System.out.println("급여 입력 내용: " + salary);
		salaryService.modifySalary(salary);
		
		return "redirect:/salary/list";
	}
	
	public boolean isEmptyString(String str) {
		return str == null || "".equals(str);
	}



	
	/* 급여 내역 삭제 */
	@GetMapping("/delete/{salNo}")
	public ModelAndView deleteSalary(RedirectAttributes rttr, ModelAndView mv, @PathVariable int salNo) {
		
		System.out.println("급여 삭제 급여번호 넘어왔나^^^^^^^^^^^^^^^^^^" + salNo);
		int result = salaryService.deleteSalary(salNo);
		
		rttr.addFlashAttribute("registSuccessMessage", "삭제되었습니다.");
		mv.setViewName("redirect:/salary/list");
		
		return mv;
		
	}
	
	
	/* 급여 등록 */
	@GetMapping("/regist")
	public void registSalary() {
		
	}
		
	@GetMapping(value = "/empName", produces = "application/json; charset = UTF-8")
	@ResponseBody
	public List<EmployeeDTO> findEmpNameList(){
		return salaryService.findEmployeeList();
	}
	
	@PostMapping("/regist")
	public ModelAndView findSalaryByNo(HttpServletRequest request, ModelAndView mv, @ModelAttribute SalaryDTO salary, RedirectAttributes rttr) {
		
		System.out.println("컨트롤러 확인 : " + salary);
		salaryService.registSalary(salary);
		
		rttr.addFlashAttribute("registSuccessMessage", "급여가 등록되었습니다.");
		mv.setViewName("redirect:/salary/list");
			
	return mv;		
	}
	
	   
}

