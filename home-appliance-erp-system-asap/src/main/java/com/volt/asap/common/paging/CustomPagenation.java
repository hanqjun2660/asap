package com.volt.asap.common.paging;

import java.util.Map;

public class CustomPagenation {

    public static CustomSelectCriteria getSelectCriteria(int pageNo, int totalCount, int limit, int buttonAmount) {
        return getSelectCriteria(pageNo, totalCount, limit, buttonAmount, null);
    }

    public static CustomSelectCriteria getSelectCriteria(int pageNo, int totalCount, int limit, int buttonAmount, Map<String, String> searchValueMap) {
        int maxPage;                    // 전체 페이지 중 가장 마지막 페이지
        int startPage;                  // 한번에 표시될 페이지 버튼 중 첫 페이지
        int endPage;                    // 한번에 표시될 페이지 버튼 중 마지막 페이지
        int startRow;
        int endRow;

        /*
            가장 마지막 페이지 계산
            전체 리스트 개수(totalCount)를 페이지당 표시될 개수(limit)로 나눠서 올림(Math.ceil())처리
         */
        maxPage = (int) Math.ceil((double) totalCount / limit);

        /*
            현재 페이지로 페이징 버튼 시작 페이지번호 계산
            현재 페이지(pageNo)를 버튼개수(buttonAmount)로 나누고 올림(Math.ceil())처리하면 버튼개수(buttonAmount) 단위가 나오고
            해당 단위에 버튼개수(buttonAmount)를 곱한뒤 첫번째 수인 1을 더해서 완성!!
         */
        startPage = (int) (Math.ceil((double) pageNo / buttonAmount) - 1) * buttonAmount + 1;

        /*
            목록 아래에 보여질 마지막 페이지 수
         */
        endPage = startPage + buttonAmount - 1;

        /* 최대 페이지가 페이징 마지막 페이지보다 작은경우 페이징 마지막 페이지가 최대 페이지 */
        if(maxPage < endPage) {
            endPage = maxPage;
        }

        /* 마지막 페이지는 0이 될 수 없기 때문에 검색 결과가 아무것도 없으면 최대 페이지와 마지막 페이지는 0으로 고정 */
        if(maxPage == 0 && endPage == 0) {
            maxPage = startPage;
            endPage = startPage;
        }

        /* 조회할 시작 번호와 마지막 행 번호 계산 */
        startRow = (pageNo - 1) * limit + 1;
        endRow = startRow + limit - 1;

        /* SelectCriteria 생성 */
        return new CustomSelectCriteria(pageNo, totalCount, limit, buttonAmount, maxPage, startPage, endPage, startRow, endRow, searchValueMap);
    }
}
