package com.volt.asap.common.handler;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 인증 실패시 핸들링하는 객체
 *
 * @see AuthenticationFailureHandler
 */
@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    /**
     * 인증 실패시 호출되는 메서드
     * 실패 메시지를 세션 스코프에 추가하고 로그인 페이지로 redirect 함
     *
     * @param request the request during which the authentication attempt occurred.
     * @param response the response.
     * @param exception the exception which was thrown to reject the authentication
     * request.
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        String message;

        if(exception instanceof BadCredentialsException) {
            message = "아아디 또는 비밀번호가 맞지 않습니다.";
        } else if(exception instanceof InternalAuthenticationServiceException) {
            message = "내부적으로 발생한 시스템 문제로 인해 로그인할 수 없습니다. 시스템 관리자에게 문의하세요.";
        } else if(exception instanceof UsernameNotFoundException) {
            message = "아아디 또는 비밀번호가 맞지 않습니다.";
        } else if(exception instanceof AuthenticationCredentialsNotFoundException) {
            message = "인증 요청이 거부되었습니다. 시스템 관리자에게 문의하세요.";
        } else {
            message = "알 수 없는 이유로 로그인에 실패하였습니다. 시스템 관리자에게 문의하세요.";
        }

        request.getSession().setAttribute("message", message);
        response.sendRedirect("/login");
    }
}
