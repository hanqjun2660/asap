package com.volt.asap.common.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 로그인 인증 성공시 핸들링하는 객체
 */
@Component
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    /**
     * 로그인 인증 성공시 호출되는 메서드
     * 아이디 저장을 했을 경우 쿠키에 저장해둠
     *
     * @param request the request which caused the successful authentication
     * @param response the response
     * @param authentication the <tt>Authentication</tt> object which was created during
     * the authentication process.
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String isSaveId = request.getParameter("saveId");

        Cookie cookie;
        if("true".equals(isSaveId)) {
            cookie = new Cookie("id", authentication.getName());
            cookie.setMaxAge(60*60*24*30);                                      // 30일
        } else {
            cookie = new Cookie("id", "");
            cookie.setMaxAge(0);
        }
        response.addCookie(cookie);

        super.onAuthenticationSuccess(request, response, authentication);
    }
}
