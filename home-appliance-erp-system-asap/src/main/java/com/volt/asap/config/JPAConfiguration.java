package com.volt.asap.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = {"com.volt.asap"})
@EnableJpaRepositories(basePackages = "com.volt.asap")
public class JPAConfiguration {

}
