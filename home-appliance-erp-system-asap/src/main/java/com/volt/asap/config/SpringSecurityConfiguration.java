package com.volt.asap.config;

import com.volt.asap.common.handler.CustomAuthenticationFailureHandler;
import com.volt.asap.common.handler.CustomAuthenticationSuccessHandler;
import com.volt.asap.login.service.LoginService;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Spring Security 설정 파일
 * 각종 권한들과 경로에 대한 설정 포함 됨
 */
@EnableWebSecurity
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final LoginService loginService;
    private final CustomAuthenticationFailureHandler failureHandler;
    private final CustomAuthenticationSuccessHandler successHandler;

    public SpringSecurityConfiguration(LoginService loginService, CustomAuthenticationFailureHandler failureHandler, CustomAuthenticationSuccessHandler successHandler) {
        this.loginService = loginService;
        this.failureHandler = failureHandler;
        this.successHandler = successHandler;
    }

    /**
     * PasswordEncoder 객체를 반환하는 메서드
     * Bean으로써 관리됨
     *
     * @return BCryptPasswordEncoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     *  시큐리티 설정을 무시할 정적 리소스를 등록하기 위해 메소드 오버라이딩
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/css/**", "/js/**", "/assets/**");
    }

    /**
     * 각종 HTTP 요청에 대한 권한 설정을 위한 메소드
     *
     * @param http the {@link HttpSecurity} to modify
     * @exception Exception csrf() 메소드 Exception 핸들링 필요함
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        /*
            CSRF: 토큰 위조 공격을 막기 위한 옵션이지만 현재 프로젝트에서는 비활성화 후 진행
         */
        http.csrf().disable().authorizeRequests()
                /* 각종 권한 설정 추가하는 부분 */
                .antMatchers("/login").permitAll()          // 로그인 페이지는 누구에게나 허용
                .antMatchers("/product/regist").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/product/modify").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/attendance/regist").hasRole("HRM_MANAGE")
                .antMatchers("/attendance/modify").hasRole("HRM_MANAGE")
                .antMatchers("/client/regist").hasRole("SALES_VIEW")
                .antMatchers("/client/registPop").hasRole("SALES_VIEW")
                .antMatchers("/client/regist").hasRole("SALES_MANAGE")
                .antMatchers("/client/registPop").hasRole("SALES_MANAGE")
                .antMatchers("/client/modifyMaintain").hasRole("SALES_MANAGE")
                .antMatchers("/client/modify").hasRole("SALES_MANAGE")
                .antMatchers("/po/regist").hasRole("SALES_VIEW")
                .antMatchers("/po/registPop").hasRole("SALES_VIEW")
                .antMatchers("/po/regist").hasRole("SALES_MANAGE")
                .antMatchers("/po/registPop").hasRole("SALES_MANAGE")
                .antMatchers("/po/modifyMaintain").hasRole("SALES_MANAGE")
                .antMatchers("/po/modify").hasRole("SALES_MANAGE")
                .antMatchers("/po/delete").hasRole("SALES_MANAGE")
                .antMatchers("/estimate/regist").hasRole("SALES_VIEW")
                .antMatchers("/estimate/registPop").hasRole("SALES_VIEW")
                .antMatchers("/estimate/regist").hasRole("SALES_MANAGE")
                .antMatchers("/estimate/registPop").hasRole("SALES_MANAGE")
                .antMatchers("/estimate/modifyMaintain").hasRole("SALES_MANAGE")
                .antMatchers("/estimate/modify").hasRole("SALES_MANAGE")
                .antMatchers("/estimate/delete").hasRole("SALES_MANAGE")
                .antMatchers("/salary/regist").hasRole("HRM_MANAGE")
                .antMatchers("/salary/modify/{salNo}").hasRole("HRM_MANAGE")
                .antMatchers("/salary/delete/{salNo}").hasRole("HRM_MANAGE")
                .antMatchers("/severancePay/regist").hasRole("HRM_MANAGE")
                .antMatchers("/severancePay/modify/{empCode}").hasRole("HRM_MANAGE")
                .antMatchers("/severancePay/delete/{empCode}").hasRole("HRM_MANAGE")
                .antMatchers("/order/{orderNumber}").hasRole("SALES_MANAGE")
                .antMatchers("/order/sheet/{orderNumber}").hasRole("SALES_MANAGE")
                .antMatchers("/sell/{sellNumber}").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/order/regist").hasRole("SALES_MANAGE")
                .antMatchers("/order/modify").hasRole("SALES_MANAGE")
                .antMatchers("/sell/regist").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/sell/modify").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/receiving/regist").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/receiving/delete").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/receiving/modify").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/receiving/registReceiving").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/receiving/regist").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/storage/delete").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/storage/modify").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/storage/regist").hasRole("LOGISTIC_MANAGE")
                .antMatchers("/notice/regist").hasRole("NOTICE_MANAGE")
                .antMatchers("/notice/{noticeNo}/modify").hasRole("NOTICE_MANAGE")
                .antMatchers("/notice/{noticeNo}/remove").hasRole("NOTICE_MANAGE")
                .antMatchers("/department/regist").hasRole("HRM_MANAGE")
                .antMatchers("/department/modifyName/{code}").hasRole("HRM_MANAGE")
                .antMatchers("/department/modifyName").hasRole("HRM_MANAGE")
                .antMatchers("/annual/regist").hasRole("HRM_MANAGE")
                .antMatchers("/annual/modify/{annualCode}").hasRole("HRM_MANAGE")
                .antMatchers("/annual/modify").hasRole("HRM_MANAGE")
                .antMatchers("/annual/confirm/{annualCode}").hasRole("HRM_MANAGE")
                .antMatchers("/employee/regist").hasRole("HRM_MANAGE")
                .antMatchers("/employee/modify/{empCode}").hasRole("HRM_MANAGE")
                .antMatchers("/employee/modify").hasRole("HRM_MANAGE")
                .antMatchers("/employee/modifyLeaveYn").hasRole("HRM_MANAGE")
                .antMatchers("/employee/issue").hasRole("HRM_MANAGE")
                .antMatchers("/employee/permission/{empCode}").hasRole("HRM_MANAGE")
                .antMatchers("/employee/permission").hasRole("HRM_MANAGE")
                .antMatchers("/employee/permission/delete/{empCode}").hasRole("HRM_MANAGE")
                .antMatchers("/employee/permission/delete").hasRole("HRM_MANAGE")
                .antMatchers("/employee/permission/modify/{empCode}").hasRole("HRM_MANAGE")
                .antMatchers("/employee/permission/modify").hasRole("HRM_MANAGE")
                .anyRequest().authenticated()               // 위에 설정되지 않은 모든 페이지는 로그인이 필요
            .and()
                .formLogin()
                .loginPage("/login")                        // 로그인에 사용할 페이지(get)와 post 요청 주소 설정
                .successHandler(successHandler)
                .failureHandler(failureHandler)             // 로그인 실패시 핸들링할 객체 등록
            .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/login")
            .and()
                .exceptionHandling()
                .accessDeniedPage("/common/error");
    }

    /**
     * 권한 등록에 필요한 비즈니스 로직이 담긴 서비스 할당
     * @param auth the {@link AuthenticationManagerBuilder} to use
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.loginService).passwordEncoder(passwordEncoder());
    }
}
