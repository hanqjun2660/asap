package com.volt.asap.receiving.repository;

import com.volt.asap.receiving.dto.ReceivingHistoryDTO;
import com.volt.asap.receiving.dto.ReceivingHistoryViewDTO;
import com.volt.asap.receiving.entity.ReceivingHistory;
import com.volt.asap.receiving.entity.ReceivingHistoryViewEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

public interface ReceivingTextRepository extends JpaRepository<ReceivingHistoryViewEntity, String> {
    int countByStorageName(String storageName);
    int countByProductName(String productName);
    List<ReceivingHistoryViewEntity> findAllByInventoryDateBetweenAndStorageNameContainingAndProductNameContainingAndReceivingStatus(java.sql.Date sDate, java.sql.Date eDate, String storageName, String productName, Pageable paging, String status);
    List<ReceivingHistoryViewEntity> findAllByProductNameContainingAndReceivingStatus(String productName, Pageable paging, String status);

    List<ReceivingHistoryViewEntity> findAllByStorageNameContainingAndReceivingStatus(String storageName, Pageable paging, String status);

    List<ReceivingHistoryViewEntity> findAllByInventoryDateBetweenAndReceivingStatus(Date startDate, Date endDate, Pageable paging, String status);

    List<ReceivingHistoryViewEntity> findAllByInventoryDateBetweenAndStorageNameContainingAndReceivingStatus(Date startDate, Date endDate, String storageName, Pageable paging, String status);

    List<ReceivingHistoryViewEntity> findAllByInventoryDateBetweenAndProductNameContainingAndReceivingStatus(Date startDate, Date endDate, String productName, Pageable paging, String status);

    List<ReceivingHistoryViewEntity> findAllByStorageNameContainingAndProductNameContainingAndReceivingStatus(String storageName, String productName, Pageable paging, String status);

    ReceivingHistoryViewEntity findByReceivingHistoryNo(Integer receivingHistoryNo);

    @Transactional
    ReceivingHistoryViewDTO deleteByReceivingHistoryNo(Integer receivingHistoryNo);

    List<ReceivingHistoryViewEntity> findByReceivingStatus(Pageable paging, String status);
}
