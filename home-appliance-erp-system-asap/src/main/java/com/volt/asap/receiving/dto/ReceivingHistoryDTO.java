package com.volt.asap.receiving.dto;

import java.sql.Date;

public class ReceivingHistoryDTO {


    private int receivingHistoryNo;
    private String receivingComent;
    private java.sql.Date inventoryDate;
    private int receivingAmount;
    private String productCode;
    private int storageNo;
    private int clientNo;

    private String receivingStatus;

    public ReceivingHistoryDTO() {
    }

    public ReceivingHistoryDTO(int receivingHistoryNo, String receivingComent, Date inventoryDate, int receivingAmount, String productCode, int storageNo, int clientNo, String receivingStatus) {
        this.receivingHistoryNo = receivingHistoryNo;
        this.receivingComent = receivingComent;
        this.inventoryDate = inventoryDate;
        this.receivingAmount = receivingAmount;
        this.productCode = productCode;
        this.storageNo = storageNo;
        this.clientNo = clientNo;
        this.receivingStatus = receivingStatus;
    }

    public int getReceivingHistoryNo() {
        return receivingHistoryNo;
    }

    public void setReceivingHistoryNo(int receivingHistoryNo) {
        this.receivingHistoryNo = receivingHistoryNo;
    }

    public String getReceivingComent() {
        return receivingComent;
    }

    public void setReceivingComent(String receivingComent) {
        this.receivingComent = receivingComent;
    }

    public Date getInventoryDate() {
        return inventoryDate;
    }

    public void setInventoryDate(Date inventoryDate) {
        this.inventoryDate = inventoryDate;
    }

    public int getReceivingAmount() {
        return receivingAmount;
    }

    public void setReceivingAmount(int receivingAmount) {
        this.receivingAmount = receivingAmount;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getStorageNo() {
        return storageNo;
    }

    public void setStorageNo(int storageNo) {
        this.storageNo = storageNo;
    }

    public int getClientNo() {
        return clientNo;
    }

    public void setClientNo(int clientNo) {
        this.clientNo = clientNo;
    }

    public String getReceivingStatus() {
        return receivingStatus;
    }

    public void setReceivingStatus(String receivingStatus) {
        this.receivingStatus = receivingStatus;
    }

    @Override
    public String toString() {
        return "ReceivingHistoryDTO{" +
                "receivingHistoryNo=" + receivingHistoryNo +
                ", receivingComent='" + receivingComent + '\'' +
                ", inventoryDate=" + inventoryDate +
                ", receivingAmount=" + receivingAmount +
                ", productCode='" + productCode + '\'' +
                ", storageNo=" + storageNo +
                ", clientNo=" + clientNo +
                ", receivingStatus='" + receivingStatus + '\'' +
                '}';
    }
}
