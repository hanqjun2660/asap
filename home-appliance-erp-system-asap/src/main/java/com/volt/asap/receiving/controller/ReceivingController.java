package com.volt.asap.receiving.controller;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.receiving.dto.ReceivingHistoryDTO;
import com.volt.asap.receiving.dto.ReceivingHistoryViewDTO;
import com.volt.asap.receiving.entity.ReceivingHistory;
import com.volt.asap.receiving.entity.ReceivingHistoryViewEntity;
import com.volt.asap.receiving.service.ReceivingService;
import com.volt.asap.sales.dto.ClientDTO;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/receiving/*")
public class ReceivingController {

    private final ReceivingService receivingService;

    public ReceivingController(ReceivingService receivingService) {
        this.receivingService = receivingService;
    }
    @GetMapping("/list")
    public ModelAndView ReceivingList(HttpServletRequest request, ModelAndView mv,
                                      Integer page, String startDate, String endDate,
                                      String storagename,
                                      String productname) {

        CustomSelectCriteria selectCriteria;

        System.out.println("시작날짜: " + startDate);
        /* 페이지 파라미터가 없으면 1페이지로 간주 */
        if (page == null) {
            page = 1;
        }

        if(isEmptyString(startDate) && isEmptyString(endDate) && isEmptyString(storagename) && isEmptyString(productname)) {
            int totalcount = receivingService.countSituation(null);
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalcount, 10, 5);

            System.out.println("초기화면 토탈카운트:" + totalcount);
            System.out.println("selectCriteria: " + selectCriteria.getPageNo());
            System.out.println("selectCriteria: " + selectCriteria.getTotalCount());
        } else {
            /* 검색 쿼리로 사용할 값들을 Map에 설정 */
            Map<String, String> parameterMap = new HashMap<>();

            parameterMap.put("startDate", startDate);
            parameterMap.put("endDate", endDate);
            parameterMap.put("storagename", storagename);
            parameterMap.put("productname", productname);

            int totalcount = receivingService.countSituation(parameterMap);

            selectCriteria = CustomPagenation.getSelectCriteria(page, totalcount, 10, 5, parameterMap);
        }

        List<ReceivingHistoryViewDTO> receivingHistoryList = receivingService.findSituation(selectCriteria);

        System.out.println("조회된 리스트: " + receivingHistoryList);

        mv.addObject("selectCriteria", selectCriteria);
        mv.addObject("receivingHistoryList", receivingHistoryList);
        mv.setViewName("receiving/list");

        return mv;
    }

    private boolean isEmptyString(String str) {                 // 넘어온 파라미터를 담은 변수가 비었는지 체크하는 메소드
        return str == null || "".equals(str);
    }

    @GetMapping("/regist")
    public ModelAndView receivingRegist(ModelAndView mv) {

        List<StorageDTO> storageList = receivingService.findStorageName();
        List<ProductDTO> productList = receivingService.findProductName();
        List<ClientDTO> clientList = receivingService.findClientName();

        mv.addObject("storageList", storageList);
        mv.addObject("productList", productList);
        mv.addObject("clientList", clientList);
        mv.setViewName("receiving/regist");
        return mv;
    }

    @PostMapping("/registReceiving")
    public ModelAndView registReceiving(
            HttpServletRequest request,
            ModelAndView mv,
            HttpServletResponse response,
            ReceivingHistoryDTO receivingHistoryDTO,
            @RequestParam("select1") String productcode,
            @RequestParam("amount") int amount,
            @RequestParam("receivingDate") Date receivingDate,
            @RequestParam("select2") int storageNo,
            @RequestParam("comment") String comment,
            @RequestParam("select3") int clientNo) throws IOException {

        receivingHistoryDTO.setProductCode(productcode);
        receivingHistoryDTO.setReceivingAmount(amount);
        receivingHistoryDTO.setInventoryDate(receivingDate);
        receivingHistoryDTO.setStorageNo(storageNo);
        receivingHistoryDTO.setReceivingComent(comment);
        receivingHistoryDTO.setClientNo(clientNo);

        int result = receivingService.saveReceivingHistory(receivingHistoryDTO);

        if(result > 0) {
            mv.addObject("message", "success");
        } else {
            mv.addObject("message", "fail");
        }

        mv.setViewName("receiving/regist");

        return mv;
    }

    @GetMapping("/{receivingNo}")
    public ModelAndView stockDetail(ModelAndView mv,  @PathVariable int receivingNo) {

        System.out.println("너는 잘 나오는게 맞지??????" + receivingNo);

        ReceivingHistoryViewDTO receivingDetail = receivingService.findReceivingDetail(receivingNo);

        mv.addObject("receivingDetail", receivingDetail);
        mv.setViewName("receiving/modify");

        return mv;
    }

    @PostMapping("/delete")
    public ModelAndView deleteReceivingHistory(ModelAndView mv, @RequestParam("receivingHistoryNo") Integer receivingHistoryNo
                                        ) {

        System.out.println("receivingHistoryNo!!!!!!!!!!!!!!!!!!!!!!!!!!!!: " + receivingHistoryNo);

        ReceivingHistory result = receivingService.deleteReceiving(receivingHistoryNo);

        if(!ObjectUtils.isEmpty(result)) {
            mv.addObject("message", "success");
            mv.addObject("comment", "삭제에 성공하였습니다.");
        } else {
            mv.addObject("message", "fail");
            mv.addObject("comment", "삭제에 실패하였습니다.");
        }

        mv.setViewName("/receiving/blank");

        return mv;
    }

    @PostMapping("/modify")
    public ModelAndView modifyRecevingHistory(ModelAndView mv,
                                              ReceivingHistoryDTO receivingHistoryDTO,
                                              @RequestParam("receivingHistoryNo") Integer receivingHistoryNo,
                                              @RequestParam("receivingAmount") Integer receivingAmount,
                                              @RequestParam("receivingComment") String receivingComment) {


        System.out.println("수량: " + receivingAmount);
        System.out.println("코멘트: " + receivingComment);

        receivingHistoryDTO.setReceivingHistoryNo(receivingHistoryNo);
        receivingHistoryDTO.setReceivingAmount(receivingAmount);
        receivingHistoryDTO.setReceivingComent(receivingComment);

        ReceivingHistory result = receivingService.modifyReceivingHistory(receivingHistoryDTO);

        if(!ObjectUtils.isEmpty(result)) {
            mv.addObject("message", "success");
            mv.addObject("comment", "수정에 성공하였습니다.");
        } else {
            mv.addObject("message", "fail");
            mv.addObject("comment", "수정에 실패하였습니다.");
        }

        mv.setViewName("/receiving/blank");

        return mv;
    }
}

