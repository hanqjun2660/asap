package com.volt.asap.groupware.entity;

import java.io.Serializable;
import java.util.Objects;

public class CompanyDateAttendId implements Serializable {
    private static final long serialVersionUID = 7626344159280925606L;
    private int empCode;
    private int dateNo;

    public CompanyDateAttendId() {
    }

    public CompanyDateAttendId(int empCode, int dateNo) {
        this.empCode = empCode;
        this.dateNo = dateNo;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public int getDateNo() {
        return dateNo;
    }

    public void setDateNo(int dateNo) {
        this.dateNo = dateNo;
    }

    @Override
    public String toString() {
        return "CompanyDateAttendId{" +
                "empCode=" + empCode +
                ", dateNo=" + dateNo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyDateAttendId that = (CompanyDateAttendId) o;
        return empCode == that.empCode && dateNo == that.dateNo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(empCode, dateNo);
    }
}
