package com.volt.asap.groupware.repository;

import com.volt.asap.groupware.entity.CompanyDateAttend;
import com.volt.asap.groupware.entity.CompanyDateAttendId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompanyDateAttendRepository extends JpaRepository<CompanyDateAttend, CompanyDateAttendId> {

    /**
     * 매개변수로 전달받은 사내 일정 번호로 참석자를 리스트로 반환하는 메서드
     * @param scheduleNo 사내 일정 번호
     * @return 조회된 참석자 리스트
     */
    List<CompanyDateAttend> findByDateNo(Integer scheduleNo);

    /**
     * 매개변수로 전달받은 사내 일정 번호에 해당하는 모든 참석자를 제거하는 메서드
     * @param scheduleNo 지우고자 하는 참석자의 사내 일정 번호
     */
    void deleteAllByDateNo(Integer scheduleNo);
}
