package com.volt.asap.groupware.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;

public class ApprovalLineDTO {
    private int empCode;
    private int draftNo;
    private int approvalOrder;
    private String approvalStatus;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private java.sql.Date approvalDate;
    private String approvalReason;
    private EmployeeDTO employee;

    public ApprovalLineDTO() {
    }

    public ApprovalLineDTO(int empCode, int draftNo, int approvalOrder, String approvalStatus, Date approvalDate, String approvalReason, EmployeeDTO employee) {
        this.empCode = empCode;
        this.draftNo = draftNo;
        this.approvalOrder = approvalOrder;
        this.approvalStatus = approvalStatus;
        this.approvalDate = approvalDate;
        this.approvalReason = approvalReason;
        this.employee = employee;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public int getDraftNo() {
        return draftNo;
    }

    public void setDraftNo(int draftNo) {
        this.draftNo = draftNo;
    }

    public int getApprovalOrder() {
        return approvalOrder;
    }

    public void setApprovalOrder(int approvalOrder) {
        this.approvalOrder = approvalOrder;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalReason() {
        return approvalReason;
    }

    public void setApprovalReason(String approvalReason) {
        this.approvalReason = approvalReason;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "ApprovalLineDTO{" +
                "empCode=" + empCode +
                ", draftNo=" + draftNo +
                ", approvalOrder=" + approvalOrder +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", approvalDate=" + approvalDate +
                ", approvalReason='" + approvalReason + '\'' +
                ", employee=" + employee +
                '}';
    }
}