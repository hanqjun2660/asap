package com.volt.asap.groupware.dto;

import java.sql.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.volt.asap.hrm.dto.EmployeeDTO;

public class NoticeDTO {

	private int noticeNo;
	private String noticeTitle;
	private String noticeContent;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date noticeDate;
	private String deleteStatus;
	private int empCode;
	
	private EmployeeDTO emp;
	private List<FileUploadDTO> attachedFileList;
	
	public NoticeDTO() {
		super();
		
	}

	public NoticeDTO(int noticeNo, String noticeTitle, String noticeContent, Date noticeDate, String deleteStatus,
			int empCode, EmployeeDTO emp, List<FileUploadDTO> attachedFileList) {
		super();
		this.noticeNo = noticeNo;
		this.noticeTitle = noticeTitle;
		this.noticeContent = noticeContent;
		this.noticeDate = noticeDate;
		this.deleteStatus = deleteStatus;
		this.empCode = empCode;
		this.emp = emp;
		this.attachedFileList = attachedFileList;
	}

	public int getNoticeNo() {
		return noticeNo;
	}

	public void setNoticeNo(int noticeNo) {
		this.noticeNo = noticeNo;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeContent() {
		return noticeContent;
	}

	public void setNoticeContent(String noticeContent) {
		this.noticeContent = noticeContent;
	}

	public Date getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public EmployeeDTO getEmp() {
		return emp;
	}

	public void setEmp(EmployeeDTO emp) {
		this.emp = emp;
	}

	public List<FileUploadDTO> getAttachedFileList() {
		return attachedFileList;
	}

	public void setAttachedFileList(List<FileUploadDTO> attachedFileList) {
		this.attachedFileList = attachedFileList;
	}

	@Override
	public String toString() {
		return "NoticeDTO [noticeNo=" + noticeNo + ", noticeTitle=" + noticeTitle + ", noticeContent=" + noticeContent
				+ ", noticeDate=" + noticeDate + ", deleteStatus=" + deleteStatus + ", empCode=" + empCode + ", emp="
				+ emp + ", attachedFileList=" + attachedFileList + "]";
	}

	
}
