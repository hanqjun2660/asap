package com.volt.asap.groupware.repository;


import com.volt.asap.hrm.entity.DepartmentMember;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentMemberRepository extends JpaRepository<DepartmentMember, String> {

    /**
     * 부서에 포함되는 인원을 부서 코드로 검색해 리스트로 반환하는 메서드
     * @param code 부서 코드
     * @return 부서에 포함되는 인원을 포함한 엔티티
     */
    public DepartmentMember findByCode(String code);

}
