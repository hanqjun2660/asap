package com.volt.asap.groupware.repository;

import com.volt.asap.groupware.entity.DraftForm;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DraftFormRepository extends JpaRepository<DraftForm, Integer> {

    /**
     * 전달받은 쿼리로 검색된 개수를 반환하는 리포지토리 메소드
     * @param type 기안서 양식의 구분
     * @param title 기안서 양식의 제목 검색어 (Contain)
     * @return 검색된 개수
     */
    int countByDraftFormDivisionAndDraftFormTitleContainingIgnoreCase(String type, String title);

    /**
     * 전달받은 쿼리로 검색된 개수를 반환하는 리포지토리 메소드
     * @param type 기안서 양식의 구분
     * @return 검색된 개수
     */
    int countByDraftFormDivision(String type);

    /**
     * 전달받은 쿼리로 검색된 개수를 반환하는 리포지토리 메소드
     * @param title 기안서 양식의 제목 검색어 (Contain)
     * @return 검색된 개수
     */
    int countByDraftFormTitleContainingIgnoreCase(String title);

    /**
     * 전달 받은 쿼리로 검색된 기안서 양식 리스트를 반환하는 리포지토리 메소드
     * @param type 기안서 양식의 구분
     * @param title 기안서 양식의 제목 검색어 (Contain)
     * @param pageable 페이징처리를 위한 {@link Pageable} 객체
     * @return 검색된 기안서 양식 리스트
     */
    List<DraftForm> findByDraftFormDivisionAndDraftFormTitleContainingIgnoreCaseOrderByDraftFormNoDesc(String type, String title, Pageable pageable);

    /**
     * 전달 받은 쿼리로 검색된 기안서 양식 리스트를 반환하는 리포지토리 메소드
     * @param type 기안서 양식의 구분
     * @param pageable 페이징처리를 위한 {@link Pageable} 객체
     * @return 검색된 기안서 양식 리스트
     */
    List<DraftForm> findByDraftFormDivisionOrderByDraftFormNoDesc(String type, Pageable pageable);

    /**
     * 전달 받은 쿼리로 검색된 기안서 양식 리스트를 반환하는 리포지토리 메소드
     * @param title 기안서 양식의 제목 검색어 (Contain)
     * @param pageable 페이징처리를 위한 {@link Pageable} 객체
     * @return 검색된 기안서 양식 리스트
     */
    List<DraftForm> findByDraftFormTitleContainingIgnoreCaseOrderByDraftFormNoDesc(String title, Pageable pageable);

    /**
     * 기안서 양식 구분을 중복 제거하고 모두 가져오는 리포지토리 메서드
     * @return 기안서 양식 구분({@link String}) 리스트
     */
    @Query(value = "SELECT DISTINCT A.DRAFT_FORM_DIVISION FROM TBL_DRAFT_FORM A", nativeQuery = true)
    List<String> findDraftFormTypeAllNative();

    /**
     * 기안서 양식을 기안서 양식 번호로 가져오는 리포지토리 메서드
     * @param draftFormNo 기안서 양식 번호
     * @return 기안서 양식
     */
    DraftForm findByDraftFormNo(int draftFormNo);
}
