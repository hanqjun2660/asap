package com.volt.asap.groupware.repository;

import com.volt.asap.groupware.entity.FlatEmployee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlatEmployeeRepository extends JpaRepository<FlatEmployee, Integer> {

}
