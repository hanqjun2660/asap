package com.volt.asap.groupware.dto;

public class DepartmentDTO {

	private String code;
	private String name;
	private String useYn;
	
	public DepartmentDTO() {
	}
	public DepartmentDTO(String code, String name, String useYn) {
		this.code = code;
		this.name = name;
		this.useYn = useYn;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	@Override
	public String toString() {
		return "DepartmentDTO [code=" + code + ", name=" + name + ", useYn=" + useYn + "]";
	}	
}
