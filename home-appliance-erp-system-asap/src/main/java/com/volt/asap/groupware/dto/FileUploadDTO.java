package com.volt.asap.groupware.dto;

import java.sql.Date;

public class FileUploadDTO {
    private int fileNo;                      // FILE_NO
    private String fileName;                 // FILE_NAME
    private String fileLocation;             // FILE_LOCATION
    private String fileUploadBoard;          // FILE_UPLOAD_BOARD
    private java.sql.Date fileUploadDate;    // FILE_UPLOAD_DATE
    private String fileRename;               // FILE_RENAME
    private Integer noticeNo;                    // NOTICE_NO
    private Integer dateNo;                      // DATE_NO

    public FileUploadDTO() {
    }

    public FileUploadDTO(int fileNo, String fileName, String fileLocation, String fileUploadBoard, Date fileUploadDate, String fileRename, Integer noticeNo, Integer dateNo) {
        this.fileNo = fileNo;
        this.fileName = fileName;
        this.fileLocation = fileLocation;
        this.fileUploadBoard = fileUploadBoard;
        this.fileUploadDate = fileUploadDate;
        this.fileRename = fileRename;
        this.noticeNo = noticeNo;
        this.dateNo = dateNo;
    }

    public int getFileNo() {
        return fileNo;
    }

    public void setFileNo(int fileNo) {
        this.fileNo = fileNo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getFileUploadBoard() {
        return fileUploadBoard;
    }

    public void setFileUploadBoard(String fileUploadBoard) {
        this.fileUploadBoard = fileUploadBoard;
    }

    public Date getFileUploadDate() {
        return fileUploadDate;
    }

    public void setFileUploadDate(Date fileUploadDate) {
        this.fileUploadDate = fileUploadDate;
    }

    public String getFileRename() {
        return fileRename;
    }

    public void setFileRename(String fileRename) {
        this.fileRename = fileRename;
    }

    public Integer getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(Integer noticeNo) {
        this.noticeNo = noticeNo;
    }

    public Integer getDateNo() {
        return dateNo;
    }

    public void setDateNo(Integer dateNo) {
        this.dateNo = dateNo;
    }

    @Override
    public String toString() {
        return "FileUploadDTO{" +
                "fileNo=" + fileNo +
                ", fileName='" + fileName + '\'' +
                ", fileLocation='" + fileLocation + '\'' +
                ", fileUploadBoard='" + fileUploadBoard + '\'' +
                ", fileUploadDate=" + fileUploadDate +
                ", fileRename='" + fileRename + '\'' +
                ", noticeNo=" + noticeNo +
                ", dateNo=" + dateNo +
                '}';
    }
}
