package com.volt.asap.groupware.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 그룹웨어 전체 페이지를 담당하는 컨트롤러
 * '/groupware'의 GET, POST 들을 담당하는 클래스
 */
@Controller
@RequestMapping("/groupware")
public class GroupwareController {

    /**
     * '/groupware'의 GET 메소드를 담당
     * @return '/groupware/draft-manage'로 redirect
     * @see org.springframework.web.servlet.ViewResolver
     */
    @GetMapping("")
    public String groupwareMain() {
        return "redirect:/groupware/draft";
    }

    /**
     * 그룹웨어를 이용하는 현재 사용자의 사원번호를 전달하는 컨트롤러 메소드 (GET)
     * @param auth 현재 로그인한 사용자의 정보가 저장되어있는 {@link Authentication} 객체
     * @return 현재 사용자의 사원번호
     */
    @ResponseBody
    @GetMapping("getEmpCode")
    public ResponseEntity<Map<String, String>> getEmpCode(Authentication auth) {
        Map<String, String> messages = new HashMap<>();
        messages.put("empCode", auth.getName());
        return ResponseEntity.ok().body(messages);
    }
}
