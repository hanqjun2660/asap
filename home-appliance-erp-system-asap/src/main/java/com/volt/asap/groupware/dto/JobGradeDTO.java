package com.volt.asap.groupware.dto;

import java.io.Serializable;

public class JobGradeDTO implements Serializable {
    private static final long serialVersionUID = -6225938503111051424L;
    private String jobCode;
    private String jobName;

    public JobGradeDTO() {
    }

    public JobGradeDTO(String jobCode, String jobName) {
        this.jobCode = jobCode;
        this.jobName = jobName;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Override
    public String toString() {
        return "JobGradeDTO{" +
                "jobCode='" + jobCode + '\'' +
                ", jobName='" + jobName + '\'' +
                '}';
    }
}
