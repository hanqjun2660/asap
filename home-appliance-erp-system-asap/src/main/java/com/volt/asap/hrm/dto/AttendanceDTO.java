package com.volt.asap.hrm.dto;

import java.sql.Date;

public class AttendanceDTO {
	
	private int attendanceCode;
	private Date workDate;
	private String goworkTime;
	private String outworkTime;
	private int workTime;
	private int overtimeAllowance;
	private int empCode;
	
	private EmployeeDTO emp;
	
	public AttendanceDTO() {
		super();
	}

	public AttendanceDTO(int attendanceCode, Date workDate, String goworkTime, String outworkTime, int workTime,
			int overtimeAllowance, int empCode, EmployeeDTO emp) {
		super();
		this.attendanceCode = attendanceCode;
		this.workDate = workDate;
		this.goworkTime = goworkTime;
		this.outworkTime = outworkTime;
		this.workTime = workTime;
		this.overtimeAllowance = overtimeAllowance;
		this.empCode = empCode;
		this.emp = emp;
	}

	public int getAttendanceCode() {
		return attendanceCode;
	}

	public void setAttendanceCode(int attendanceCode) {
		this.attendanceCode = attendanceCode;
	}

	public Date getWorkDate() {
		return workDate;
	}

	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}

	public String getGoworkTime() {
		return goworkTime;
	}

	public void setGoworkTime(String goworkTime) {
		this.goworkTime = goworkTime;
	}

	public String getOutworkTime() {
		return outworkTime;
	}

	public void setOutworkTime(String outworkTime) {
		this.outworkTime = outworkTime;
	}

	public int getWorkTime() {
		return workTime;
	}

	public void setWorkTime(int workTime) {
		this.workTime = workTime;
	}

	public int getOvertimeAllowance() {
		return overtimeAllowance;
	}

	public void setOvertimeAllowance(int overtimeAllowance) {
		this.overtimeAllowance = overtimeAllowance;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public EmployeeDTO getEmp() {
		return emp;
	}

	public void setEmp(EmployeeDTO emp) {
		this.emp = emp;
	}

	@Override
	public String toString() {
		return "AttendanceDTO [attendanceCode=" + attendanceCode + ", workDate=" + workDate + ", goworkTime="
				+ goworkTime + ", outworkTime=" + outworkTime + ", workTime=" + workTime + ", overtimeAllowance="
				+ overtimeAllowance + ", empCode=" + empCode + ", emp=" + emp + "]";
	}

	
	
	
}
