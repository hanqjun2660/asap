package com.volt.asap.hrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.JobGrade;

public interface JobGradeRepository extends JpaRepository<JobGrade, Integer> {

}
