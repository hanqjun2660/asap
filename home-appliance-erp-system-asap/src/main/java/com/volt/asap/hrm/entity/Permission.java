package com.volt.asap.hrm.entity;

import javax.persistence.*;

/**
 * 권한 정보가 담겨있는 Entity
 *
 * @see com.volt.asap.login.dto.PermissionDTO
 */
@Entity(name = "Permission")
@Table(name = "TBL_PERMISSION")
@SequenceGenerator(
        name = "PERMISSION_CODE_SEQ_GENERATOR",
        sequenceName = "SEQ_PERMISSION_CODE",
        initialValue = 15,
        allocationSize = 1
)
public class Permission {

    @Id
    @Column(name = "PERMISSION_CODE")
    private String permissionCode;                  // PERMISSION_CODE

    @Column(name = "PERMISSION_NAME")
    private String permissionName;                  // PERMISSION_NAME

    public Permission() { }

    public Permission(String permissionCode, String permissionName) {
        this.permissionCode = permissionCode;
        this.permissionName = permissionName;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public Permission setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
        return this;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public Permission setPermissionName(String permissionName) {
        this.permissionName = permissionName;
        return this;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "permissionCode='" + permissionCode + '\'' +
                ", permissionName='" + permissionName + '\'' +
                '}';
    }
}
