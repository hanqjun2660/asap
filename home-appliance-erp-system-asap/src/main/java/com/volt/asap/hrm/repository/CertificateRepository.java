package com.volt.asap.hrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.Certificate;

public interface CertificateRepository extends JpaRepository<Certificate, Integer> {

}
