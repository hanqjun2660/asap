package com.volt.asap.hrm.dto;

public class PermissionDTO {

	private String permissionCode;
	private String permissionName;
	
	public PermissionDTO() {
	}
	public PermissionDTO(String permissionCode, String permissionName) {
		this.permissionCode = permissionCode;
		this.permissionName = permissionName;
	}
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	@Override
	public String toString() {
		return "PermissionDTO [permissionCode=" + permissionCode + ", permissionName=" + permissionName + "]";
	}
}
