package com.volt.asap.hrm.dto;

import java.io.Serializable;

public class UserPermissionDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private int empCode;
	private String permissionCode;
	private PermissionDTO permission;
	private EmployeeDTO employee;
	
	public UserPermissionDTO() {
	}

	public UserPermissionDTO(int empCode, String permissionCode, PermissionDTO permission, EmployeeDTO employee) {
		this.empCode = empCode;
		this.permissionCode = permissionCode;
		this.permission = permission;
		this.employee = employee;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public PermissionDTO getPermission() {
		return permission;
	}

	public void setPermission(PermissionDTO permission) {
		this.permission = permission;
	}

	public EmployeeDTO getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeDTO employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "UserPermissionDTO [empCode=" + empCode + ", permissionCode=" + permissionCode + ", permission="
				+ permission + ", employee=" + employee + "]";
	}

	
}
