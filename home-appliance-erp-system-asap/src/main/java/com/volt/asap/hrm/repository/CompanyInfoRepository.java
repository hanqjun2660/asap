package com.volt.asap.hrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.CompanyInfo;

public interface CompanyInfoRepository extends JpaRepository<CompanyInfo, String> {

	CompanyInfo findCompanyInfoByCompanyName(String companyName);

}
