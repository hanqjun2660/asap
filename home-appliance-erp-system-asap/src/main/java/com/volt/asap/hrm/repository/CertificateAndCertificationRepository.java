package com.volt.asap.hrm.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.CertificateAndCertification;

public interface CertificateAndCertificationRepository extends JpaRepository<CertificateAndCertification, Integer>{

	CertificateAndCertification findCertificateByCertificateCode(int certificateCode);

	int countByEmpNameContaining(String searchValue);

	List<CertificateAndCertification> findByEmpNameContaining(String searchValue, Pageable paging);

}
