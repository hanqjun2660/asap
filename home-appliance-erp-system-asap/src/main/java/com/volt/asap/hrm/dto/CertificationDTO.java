package com.volt.asap.hrm.dto;

public class CertificationDTO {

	private String certificateTypeCode;
	private String certificateType;
	
	public CertificationDTO() {
	}
	public CertificationDTO(String certificateTypeCode, String certificateType) {
		this.certificateTypeCode = certificateTypeCode;
		this.certificateType = certificateType;
	}
	public String getCertificateTypeCode() {
		return certificateTypeCode;
	}
	public void setCertificateTypeCode(String certificateTypeCode) {
		this.certificateTypeCode = certificateTypeCode;
	}
	public String getCertificateType() {
		return certificateType;
	}
	public void setCertificateType(String certificateType) {
		this.certificateType = certificateType;
	}
	@Override
	public String toString() {
		return "CertificationDTO [certificateTypeCode=" + certificateTypeCode + ", certificateType=" + certificateType
				+ "]";
	}
}
