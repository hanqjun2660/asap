package com.volt.asap.hrm.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.hrm.dto.DepartmentDTO;
import com.volt.asap.hrm.entity.Department;
import com.volt.asap.hrm.repository.DepartmentRepository;

@Service
public class DepartmentService {

	private final DepartmentRepository departmentRepository;
	private final ModelMapper modelMapper;
	
	@Autowired
	public DepartmentService(DepartmentRepository departmentRepository, ModelMapper modelMapper) {
		this.departmentRepository = departmentRepository;
		this.modelMapper = modelMapper;
	}
	
	/* 부서 목록 조회 */
	public List<DepartmentDTO> findDepartmentList() {
		
		List<Department> deptList = departmentRepository.findAll(Sort.by("code"));
		
		System.out.println("서비스 부서 목록 조회 : " + deptList);
		
		return deptList.stream().map(department -> modelMapper.map(department, DepartmentDTO.class)).collect(Collectors.toList());
	}

	/* 신규부서 등록 */
	@Transactional
	public void registDepartment(DepartmentDTO department) {
		
		System.out.println("부서코드 : " + department.getCode() + ", 부서명 : " + department.getName() + ", 사용여부 : " + department.getUseYn());
		
//		departmentRepository.save(modelMapper.map(department, Department.class));
		String name = department.getName();
		String useYn = department.getUseYn();
		int result = departmentRepository.insertQuery(name, useYn);
		System.out.println("네이티브 쿼리로 insert 성공?: " + result);
	}

	public DepartmentDTO findDepartmentByCode(String code) {
	
		Department department = departmentRepository.findByCodeLike(code);
		
		return modelMapper.map(department, DepartmentDTO.class);
	}

	/* 부서명, 부서 사용 상태 수정 */
	@Transactional
	public void modifyDepartment(DepartmentDTO department) {
	
		Department dept = departmentRepository.findByCodeLike(department.getCode());
		dept.setName(department.getName());
		dept.setUseYn(department.getUseYn());
	}
}
