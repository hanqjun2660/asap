package com.volt.asap.hrm.dto;

public class CertificateDTO {

	private int certificateCode;
	private String empName;
	private String purpose;
	private String publishedDate;
	private int empCode;
	private String certificateTypeCode;
	private CertificationDTO certification;
	
	public CertificateDTO() {
	}

	public CertificateDTO(int certificateCode, String empName, String purpose, String publishedDate, int empCode,
			String certificateTypeCode, CertificationDTO certification) {
		this.certificateCode = certificateCode;
		this.empName = empName;
		this.purpose = purpose;
		this.publishedDate = publishedDate;
		this.empCode = empCode;
		this.certificateTypeCode = certificateTypeCode;
		this.certification = certification;
	}

	public int getCertificateCode() {
		return certificateCode;
	}

	public void setCertificateCode(int certificateCode) {
		this.certificateCode = certificateCode;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public String getCertificateTypeCode() {
		return certificateTypeCode;
	}

	public void setCertificateTypeCode(String certificateTypeCode) {
		this.certificateTypeCode = certificateTypeCode;
	}

	public CertificationDTO getCertification() {
		return certification;
	}

	public void setCertification(CertificationDTO certification) {
		this.certification = certification;
	}

	@Override
	public String toString() {
		return "CertificateDTO [certificateCode=" + certificateCode + ", empName=" + empName + ", purpose=" + purpose
				+ ", publishedDate=" + publishedDate + ", empCode=" + empCode + ", certificateTypeCode="
				+ certificateTypeCode + ", certification=" + certification + "]";
	}
}
