package com.volt.asap.hrm.dto;

import java.sql.Date;

public class AnnualDTO {

	private int annualCode;
	private String annualName;
	private String deptCode;
	private java.sql.Date annualStart;
	private int annualLeaveDat;
	private int empCode;
	private java.sql.Date annualEnd;
	private String annualYn;
	private EmployeeDTO employee;
	
	public AnnualDTO() {
	}

	public AnnualDTO(int annualCode, String annualName, String deptCode, Date annualStart, int annualLeaveDat,
			int empCode, Date annualEnd, String annualYn, EmployeeDTO employee) {
		this.annualCode = annualCode;
		this.annualName = annualName;
		this.deptCode = deptCode;
		this.annualStart = annualStart;
		this.annualLeaveDat = annualLeaveDat;
		this.empCode = empCode;
		this.annualEnd = annualEnd;
		this.annualYn = annualYn;
		this.employee = employee;
	}

	public int getAnnualCode() {
		return annualCode;
	}

	public void setAnnualCode(int annualCode) {
		this.annualCode = annualCode;
	}

	public String getAnnualName() {
		return annualName;
	}

	public void setAnnualName(String annualName) {
		this.annualName = annualName;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public java.sql.Date getAnnualStart() {
		return annualStart;
	}

	public void setAnnualStart(java.sql.Date annualStart) {
		this.annualStart = annualStart;
	}

	public int getAnnualLeaveDat() {
		return annualLeaveDat;
	}

	public void setAnnualLeaveDat(int annualLeaveDat) {
		this.annualLeaveDat = annualLeaveDat;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public java.sql.Date getAnnualEnd() {
		return annualEnd;
	}

	public void setAnnualEnd(java.sql.Date annualEnd) {
		this.annualEnd = annualEnd;
	}

	public String getAnnualYn() {
		return annualYn;
	}

	public void setAnnualYn(String annualYn) {
		this.annualYn = annualYn;
	}

	public EmployeeDTO getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeDTO employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "AnnualDTO [annualCode=" + annualCode + ", annualName=" + annualName + ", deptCode=" + deptCode
				+ ", annualStart=" + annualStart + ", annualLeaveDat=" + annualLeaveDat + ", empCode=" + empCode
				+ ", annualEnd=" + annualEnd + ", annualYn=" + annualYn + ", employee=" + employee + "]";
	}
}
