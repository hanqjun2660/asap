package com.volt.asap.hrm.entity;

import javax.persistence.*;

@Entity(name = "CertificateAndCertification")
@Table(name = "TBL_CERTIFICATE")

public class CertificateAndCertification {

	@Id
	@Column(name = "CERTIFICATE_CODE")
	private int certificateCode;
	
	@Column(name = "EMP_NAME")
	private String empName;
	
	@Column(name = "PURPOSE")
	private String purpose;
	
	@Column(name = "PUBLISHED_DATE")
	private String publishedDate;
	
	@Column(name = "EMP_CODE")
	private int empCode;
	
	@ManyToOne
	@JoinColumn(name = "CERTIFICATE_TYPE_CODE")
	private Certification certification;

	public CertificateAndCertification() {
	}

	public CertificateAndCertification(int certificateCode, String empName, String purpose, String publishedDate,
			int empCode, Certification certification) {
		this.certificateCode = certificateCode;
		this.empName = empName;
		this.purpose = purpose;
		this.publishedDate = publishedDate;
		this.empCode = empCode;
		this.certification = certification;
	}

	public int getCertificateCode() {
		return certificateCode;
	}

	public void setCertificateCode(int certificateCode) {
		this.certificateCode = certificateCode;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public Certification getCertification() {
		return certification;
	}

	public void setCertification(Certification certification) {
		this.certification = certification;
	}

	@Override
	public String toString() {
		return "CertificateAndCertification [certificateCode=" + certificateCode + ", empName=" + empName + ", purpose="
				+ purpose + ", publishedDate=" + publishedDate + ", empCode=" + empCode + ", certification="
				+ certification + "]";
	}
}
