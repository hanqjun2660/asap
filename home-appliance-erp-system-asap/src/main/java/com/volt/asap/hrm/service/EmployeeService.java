package com.volt.asap.hrm.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.volt.asap.receiving.dto.ReceivingHistoryViewDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.SelectCriteria;
import com.volt.asap.hrm.dto.CertificateDTO;
import com.volt.asap.hrm.dto.CertificationDTO;
import com.volt.asap.hrm.dto.CompanyInfoDTO;
import com.volt.asap.hrm.dto.DepartmentDTO;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.hrm.dto.JobGradeDTO;
import com.volt.asap.hrm.dto.PermissionDTO;
import com.volt.asap.hrm.dto.UserPermissionDTO;
import com.volt.asap.hrm.dto.UserPermissionPK;
import com.volt.asap.hrm.entity.Certificate;
import com.volt.asap.hrm.entity.CertificateAndCertification;
import com.volt.asap.hrm.entity.Certification;
import com.volt.asap.hrm.entity.CompanyInfo;
import com.volt.asap.hrm.entity.Department;
import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.hrm.entity.EmployeeAndJobAndDepartment;
import com.volt.asap.hrm.entity.JobGrade;
import com.volt.asap.hrm.entity.Permission;
import com.volt.asap.hrm.entity.UserPermission;
import com.volt.asap.hrm.entity.UserPermissionInsert;
import com.volt.asap.hrm.repository.CertificateAndCertificationRepository;
import com.volt.asap.hrm.repository.CertificateRepository;
import com.volt.asap.hrm.repository.CertificationRepository;
import com.volt.asap.hrm.repository.CompanyInfoRepository;
import com.volt.asap.hrm.repository.DepartmentRepository;
import com.volt.asap.hrm.repository.EmployeeAndJobAndDepartmentRepository;
import com.volt.asap.hrm.repository.EmployeeRepository;
import com.volt.asap.hrm.repository.JobGradeRepository;
import com.volt.asap.hrm.repository.PermissionRepository;
import com.volt.asap.hrm.repository.UserPermissionInsertRepository;
import com.volt.asap.hrm.repository.UserPermissionRepository;

@Service
public class EmployeeService {

	private final EmployeeRepository employeeRepository;
	private final DepartmentRepository departmentRepository;
	private final JobGradeRepository jobGradeRepository;
	private final EmployeeAndJobAndDepartmentRepository employeeAndJobAndDepartmentRepository;
	private final PermissionRepository permissionRepository;
	private final UserPermissionInsertRepository userPermissionInsertRepository;
	private final CertificateAndCertificationRepository certificateAndCertificationRepository;
	private final CompanyInfoRepository companyInfoRepository;
	private final CertificationRepository certificationRepository;
	private final CertificateRepository certificateRepository;
	private final UserPermissionRepository userPermissionRepository;
	private final ModelMapper modelMapper;
	
	@Autowired
	public EmployeeService(EmployeeRepository employeeRepository, ModelMapper modelMapper
			, DepartmentRepository departmentRepository, JobGradeRepository jobGradeRepository
			, EmployeeAndJobAndDepartmentRepository employeeAndJobAndDepartmentRepository
			, PermissionRepository permissionRepository, UserPermissionInsertRepository userPermissionInsertRepository
			, CertificateAndCertificationRepository certificateAndCertificationRepository
			, CompanyInfoRepository companyInfoRepository, CertificationRepository certificationRepository
			, CertificateRepository certificateRepository, UserPermissionRepository userPermissionRepository) {
		this.employeeRepository = employeeRepository;
		this.modelMapper = modelMapper;
		this.departmentRepository = departmentRepository;
		this.jobGradeRepository = jobGradeRepository;
		this.employeeAndJobAndDepartmentRepository = employeeAndJobAndDepartmentRepository;
		this.permissionRepository = permissionRepository;
		this.userPermissionInsertRepository = userPermissionInsertRepository;
		this.certificateAndCertificationRepository = certificateAndCertificationRepository;
		this.companyInfoRepository = companyInfoRepository;
		this.certificationRepository = certificationRepository;
		this.certificateRepository = certificateRepository;
		this.userPermissionRepository = userPermissionRepository;
	}
	
	/* 사원 목록 조회 */
	public List<EmployeeDTO> findEmployeeList() {
		
		List<EmployeeAndJobAndDepartment> empList = employeeAndJobAndDepartmentRepository.findAll(Sort.by("empCode"));
		
		return empList.stream().map(employeeAndJobAndDepartmentRepository -> modelMapper.map(employeeAndJobAndDepartmentRepository, EmployeeDTO.class)).collect(Collectors.toList());
	}

	/* 부서 목록 조회 */
	public List<DepartmentDTO> findDepartmentList() {
		
		List<Department> deptList = departmentRepository.findAll();
		
		return deptList.stream().map(department -> modelMapper.map(department, DepartmentDTO.class)).collect(Collectors.toList());
	}

	/* 직급 목록 조회 */
	public List<JobGradeDTO> findJobList() {
		
		List<JobGrade> jobList = jobGradeRepository.findAll();
		
		return jobList.stream().map(jobGrade -> modelMapper.map(jobGrade, JobGradeDTO.class)).collect(Collectors.toList());
	}

	/* 사원 목록 페이징을 위해 게시글 수 조회 */
	public int selectTotalCount(String searchCondition, String searchValue) {
		
		int count = 0;
		
		if(searchValue != null) {
			if("empName".equals(searchCondition)) {
				count = employeeAndJobAndDepartmentRepository.countByEmpNameContaining(searchValue);
			}
			if("deptName".equals(searchCondition)) {
				count = employeeAndJobAndDepartmentRepository.countByDepartmentNameContaining(searchValue);
			}
			if("jobName".equals(searchCondition)) {
				count = employeeAndJobAndDepartmentRepository.countByJobGradeJobNameContaining(searchValue);
			}
		} else {
			count = (int)employeeAndJobAndDepartmentRepository.count();
			System.out.println("count: " + count);
		}
		return count;
	}

	/* 사원 목록 조회(검색조건별로) */
	public List<EmployeeDTO> findEmployeeList(SelectCriteria selectCriteria) {
		int index = selectCriteria.getPageNo() - 1;
		int count = selectCriteria.getLimit();
		String searchValue = selectCriteria.getSearchValue();

		/* 페이징 처리와 정렬을 위한 객체 생성 */
		Pageable paging = PageRequest.of(index, count, Sort.by("empLeaveYn").and(Sort.by("empCode").descending()));

		List<EmployeeAndJobAndDepartment> empList = new ArrayList<EmployeeAndJobAndDepartment>();
		
		System.out.println("SearchCondition : " + selectCriteria.getSearchCondition());
		System.out.println("SearchValue" + selectCriteria.getSearchValue());
		
		if(searchValue != null) {

			/* 이름 검색일 경우 */
			if("empName".equals(selectCriteria.getSearchCondition())) {
				empList = employeeAndJobAndDepartmentRepository.findByEmpNameContaining(selectCriteria.getSearchValue(), paging);
			}

			/* 부서 검색일 경우 */
			if("deptName".equals(selectCriteria.getSearchCondition())) {
				empList = employeeAndJobAndDepartmentRepository.findByDepartmentNameContaining(selectCriteria.getSearchValue(), paging);
			}
			
			/* 직급 검색일 경우 */
			if("jobName".equals(selectCriteria.getSearchCondition())) {
				empList = employeeAndJobAndDepartmentRepository.findByJobGradeJobNameContaining(selectCriteria.getSearchValue(), paging);
			}
		} else {
			empList = employeeAndJobAndDepartmentRepository.findAll(paging).toList();
		}

		return empList.stream().map(employeeAndJobAndDepartmentRepository -> modelMapper.map(employeeAndJobAndDepartmentRepository, EmployeeDTO.class)).collect(Collectors.toList());
	}

	/* 사원 상세 정보 조회 */
	public EmployeeDTO findEmployeeByEmpCode(int empCode) {
		
		EmployeeAndJobAndDepartment employeeAndJobAndDepartment = employeeAndJobAndDepartmentRepository.findEmployeeByEmpCode(empCode);
		return modelMapper.map(employeeAndJobAndDepartment, EmployeeDTO.class);
	}

	/* 부서 전체 조회 */
	public List<DepartmentDTO> findAllDepartment() {
		
		List<Department> deptList = departmentRepository.findAll();
		return deptList.stream().map(department -> modelMapper.map(department, DepartmentDTO.class)).collect(Collectors.toList());
	}

	/* 직급 전체 조회 */
	public List<JobGradeDTO> findJobGradeList() {
		
		List<JobGrade> jobList = jobGradeRepository.findAll();
		return jobList.stream().map(jobGrade -> modelMapper.map(jobGrade, JobGradeDTO.class)).collect(Collectors.toList());
	}

	/* 신규 사원 등록 */
	@Transactional
	public void registEmployee(EmployeeDTO newEmployee) {
		
		employeeRepository.save(modelMapper.map(newEmployee, Employee.class));
	}

	/* 사원 정보 수정 */
	@Transactional
	public void modifyEmployee(EmployeeDTO employee) {

		Employee modifyEmployee = employeeRepository.findByEmpCodeLike(employee.getEmpCode());
		
		modifyEmployee.setEmpName(employee.getEmpName());
		modifyEmployee.setEmpEmail(employee.getEmpEmail());
		modifyEmployee.setEmpJoindate(employee.getEmpJoindate());
		modifyEmployee.setEmpBirth(employee.getEmpBirth());
		modifyEmployee.setEmpGender(employee.getEmpGender());
		modifyEmployee.setEmpPhone(employee.getEmpPhone());
		modifyEmployee.setEmpAddress(employee.getEmpAddress());
		modifyEmployee.setDeptCode(employee.getDeptCode());
		modifyEmployee.setJobCode(employee.getJobCode());
		modifyEmployee.setEmpLeaveYn(employee.getEmpLeaveYn());
		
		/* 프로필 사진이 비어있지 않을 때만 수정 */
		if(employee.getProfileName() != null) {
			modifyEmployee.setProfileName(employee.getProfileName());
			modifyEmployee.setProfileRename(employee.getProfileRename());
			modifyEmployee.setProfilePath(employee.getProfilePath());
		}
	}

	/* 퇴사 신청자 목록 조회 */
	public List<EmployeeDTO> findLeaveList(String leaveYn) {
		
		List<EmployeeAndJobAndDepartment> leaveList = employeeAndJobAndDepartmentRepository.findByEmpLeaveYnLike(leaveYn);
		
		return leaveList.stream().map(employeeAndJobAndDepartmentRepository -> modelMapper.map(employeeAndJobAndDepartmentRepository, EmployeeDTO.class)).collect(Collectors.toList());
	}

	/* 퇴사 신청 승인 처리 */
	@Transactional
	public void modifyEmployeeLeave(EmployeeDTO employee) {
		
		System.out.println("퇴사승인 서비스 도착 : " + employee);
		Employee modifyEmployee = employeeRepository.findByEmpCodeLike(employee.getEmpCode());
		
		modifyEmployee.setEmpLeavedate(employee.getEmpLeavedate());
		modifyEmployee.setEmpLeaveYn(employee.getEmpLeaveYn());
	}

	/* 증명서 발급 목록 페이징을 위한 게시글 수 조회 */
	public int selectCertificateTotalCount(String searchCondition, String searchValue) {
		int count = 0;
		
		if(searchValue != null) {
			if("empName".equals(searchCondition)) {
				count = certificateAndCertificationRepository.countByEmpNameContaining(searchValue);
			}
		} else {
			count = (int)certificateAndCertificationRepository.count();
			System.out.println("count: " + count);
		}
		return count;
	}
	
	/* 증명서 발급 내역 조회 */
	public List<CertificateDTO> findCertificateList(SelectCriteria selectCriteria) {
		int index = selectCriteria.getPageNo() - 1;
		int count = selectCriteria.getLimit();
		String searchValue = selectCriteria.getSearchValue();

		/* 페이징 처리와 정렬을 위한 객체 생성 */
		Pageable paging = PageRequest.of(index, count, Sort.by("certificateCode").descending());
		
		List<CertificateAndCertification> certificateList = new ArrayList<CertificateAndCertification>();
		
		if(searchValue != null) {
			if("empName".equals(selectCriteria.getSearchCondition())) {
				certificateList = certificateAndCertificationRepository.findByEmpNameContaining(selectCriteria.getSearchValue(), paging);
			}
		} else {
			certificateList = certificateAndCertificationRepository.findAll(paging).toList();
		}
		
		System.out.println("서비스에서 증명서 발급 내역 조회 : " + certificateList);
		
		return certificateList.stream().map(certificateAndCertificationRepository -> modelMapper.map(certificateAndCertificationRepository, CertificateDTO.class)).collect(Collectors.toList());
	}

	/* 증명서 상세 정보 조회 */
	public CertificateDTO findCertificateByCertificateCode(int certificateCode) {
		CertificateAndCertification certificateAndCertification = certificateAndCertificationRepository.findCertificateByCertificateCode(certificateCode);
		
		return modelMapper.map(certificateAndCertification, CertificateDTO.class);
	}

	/* 회사 정보 조회 */
	public CompanyInfoDTO findCompanyInfo(String companyName) {
		
		CompanyInfo companyInfo = companyInfoRepository.findCompanyInfoByCompanyName(companyName);
		
		return modelMapper.map(companyInfo, CompanyInfoDTO.class);
	}

	/* 증명서 관련 조회 */
	public List<CertificationDTO> findCertificationList() {
		
		List<Certification> certificationList = certificationRepository.findAll();
		
		return certificationList.stream().map(certificationRepository -> modelMapper.map(certificationRepository, CertificationDTO.class)).collect(Collectors.toList());
	}

	/* 증명서 발급 등록 */
	@Transactional
	public void registCertificate(CertificateDTO certificate) {
		
		certificateRepository.save(modelMapper.map(certificate, Certificate.class));
		
	}

	/* 선택한 이름을 통해서 사원 조회 */
	public EmployeeDTO findEmployeeByEmpName(String empName) {
		
		Employee employee = employeeRepository.findByEmpName(empName);
		
		return modelMapper.map(employee, EmployeeDTO.class);
	}

	/* 권한 조회 */
	public List<PermissionDTO> findpermissionList() {
		
		List<Permission> permissionList = permissionRepository.findAll();
		
		return permissionList.stream().map(permission -> modelMapper.map(permission, PermissionDTO.class)).collect(Collectors.toList());
	}

	/* 권한 등록 */
	public void registUserPermission(UserPermissionDTO userPermission) {
		
		System.out.println("권한 정보 서비스 도착 : " + userPermission);
		
		userPermissionInsertRepository.save(modelMapper.map(userPermission, UserPermissionInsert.class));
	}
	
	/* 사원 권한 삭제를 위한 조회 */
	public List<UserPermissionDTO> findUserPermissionByCode(int empCode) {
		
		List<UserPermission> userPermissionList = userPermissionRepository.findByEmpCode(empCode);
		
		return userPermissionList.stream().map(userPermissionRepository -> modelMapper.map(userPermissionRepository, UserPermissionDTO.class)).collect(Collectors.toList());
	}

	/* 사원 권한 삭제 */
	public int deleteUserPermission(UserPermissionPK userPermission) {
		
		int result = 0;
		int empCode = userPermission.getEmpCode();
		String permissionCode = userPermission.getPermissionCode();
		
		result = userPermissionInsertRepository.deleteByEmpCodeAndPermissionCode(empCode, permissionCode);
		System.out.println("삭제 성공? : " + result);
		
		return result;
	}

	/* 권한 관련 수정 */
	@Transactional
	public void modifyUserPermission(UserPermissionPK userPermission, String code) {

		int empCode = userPermission.getEmpCode();
		String permissionCode = userPermission.getPermissionCode();
		int result = userPermissionInsertRepository.modifyQuery(empCode, permissionCode, code);
		System.out.println("수정 성공?? " + result);
	}

    public List<EmployeeDTO> findEmployeeByDeptNo(String dept) {
		List<Employee> employeeList;
		try {
			employeeList = employeeRepository.findByDeptCodeAndEmpLeaveYn(dept, "N");
		} catch (IllegalArgumentException e) {
			return null;
		}

		return employeeList.stream().map(
				employee -> modelMapper.map(employee, EmployeeDTO.class
				)).collect(Collectors.toList());
    }

	public EmployeeDTO findById(int empCode) {
		EmployeeAndJobAndDepartment employee;

		try {
			employee = employeeAndJobAndDepartmentRepository.findByEmpCodeAndEmpLeaveYn(empCode, "N");
			if(employee == null) {
				return null;
			}
		} catch (IllegalArgumentException e) {
			return null;
		}

		return modelMapper.map(employee, EmployeeDTO.class);
	}

	@Transactional
	public Employee findByEmpCode(String encodepass, int code) {

		Employee result;

		result = employeeRepository.findByEmpCode(code);

		result.setEmpPwd(encodepass);

		return result;
	}
}
