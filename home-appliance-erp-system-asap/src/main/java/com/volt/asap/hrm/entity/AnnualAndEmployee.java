package com.volt.asap.hrm.entity;

import java.sql.Date;

import javax.persistence.*;

@Entity(name = "AnnualAndEmployee")
@Table(name = "TBL_ANNUAL")
public class AnnualAndEmployee {

	@Id
	@Column(name = "ANNUAL_CODE")
	private int annualCode;
	
	@Column(name = "ANNUAL_NAME")
	private String annualName;
	
	@Column(name = "DEPT_CODE")
	private String deptCode;
	
	@Column(name = "ANNUAL_START")
	private java.sql.Date annualStart;
	
	@Column(name = "ANNUAL_LEAVE_DAT")
	private int annualLeaveDat;
	
	@ManyToOne
	@JoinColumn(name = "EMP_CODE")
	private EmployeeAndJobAndDepartment employee;
	
	@Column(name = "ANNUAL_END")
	private java.sql.Date annualEnd;
	
	@Column(name = "ANNUAL_YN")
	private String annualYn;

	public AnnualAndEmployee() {
	}

	public AnnualAndEmployee(int annualCode, String annualName, String deptCode, Date annualStart, int annualLeaveDat,
			EmployeeAndJobAndDepartment employee, Date annualEnd, String annualYn) {
		this.annualCode = annualCode;
		this.annualName = annualName;
		this.deptCode = deptCode;
		this.annualStart = annualStart;
		this.annualLeaveDat = annualLeaveDat;
		this.employee = employee;
		this.annualEnd = annualEnd;
		this.annualYn = annualYn;
	}

	public int getAnnualCode() {
		return annualCode;
	}

	public void setAnnualCode(int annualCode) {
		this.annualCode = annualCode;
	}

	public String getAnnualName() {
		return annualName;
	}

	public void setAnnualName(String annualName) {
		this.annualName = annualName;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public java.sql.Date getAnnualStart() {
		return annualStart;
	}

	public void setAnnualStart(java.sql.Date annualStart) {
		this.annualStart = annualStart;
	}

	public int getAnnualLeaveDat() {
		return annualLeaveDat;
	}

	public void setAnnualLeaveDat(int annualLeaveDat) {
		this.annualLeaveDat = annualLeaveDat;
	}

	public EmployeeAndJobAndDepartment getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeAndJobAndDepartment employee) {
		this.employee = employee;
	}

	public java.sql.Date getAnnualEnd() {
		return annualEnd;
	}

	public void setAnnualEnd(java.sql.Date annualEnd) {
		this.annualEnd = annualEnd;
	}

	public String getAnnualYn() {
		return annualYn;
	}

	public void setAnnualYn(String annualYn) {
		this.annualYn = annualYn;
	}

	@Override
	public String toString() {
		return "AnnualAndEmployee [annualCode=" + annualCode + ", annualName=" + annualName + ", deptCode=" + deptCode
				+ ", annualStart=" + annualStart + ", annualLeaveDat=" + annualLeaveDat + ", employee=" + employee
				+ ", annualEnd=" + annualEnd + ", annualYn=" + annualYn + "]";
	}
}
