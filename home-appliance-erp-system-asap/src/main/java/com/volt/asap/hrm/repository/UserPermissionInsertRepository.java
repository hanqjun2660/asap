package com.volt.asap.hrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.volt.asap.hrm.dto.UserPermissionPK;
import com.volt.asap.hrm.entity.UserPermissionInsert;

public interface UserPermissionInsertRepository extends JpaRepository<UserPermissionInsert, UserPermissionPK> {

	@Transactional
	int deleteByEmpCodeAndPermissionCode(int empCode, String permissionCode);

	UserPermissionInsert findByEmpCodeAndPermissionCode(int empCode, String permissionCode);

	@Modifying
	@Transactional
	@Query(value="UPDATE\r\n"
			+ "       TBL_USER_PERMISSION A\r\n"
			+ "   SET A.PERMISSION_CODE = :code\r\n"
			+ " WHERE A.EMP_CODE = :empCode\r\n"
			+ "   AND A.PERMISSION_CODE = :permissionCode", nativeQuery = true)
	int modifyQuery(int empCode, String permissionCode, String code);

}
