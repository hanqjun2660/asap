package com.volt.asap.hrm.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.volt.asap.hrm.entity.Department;

public interface DepartmentRepository extends JpaRepository<Department, String>{

	Department findByCodeLike(String code);

	@Modifying
	@Transactional
	@Query(value="INSERT INTO TBL_DEPT(DEPT_CODE,DEPT_NAME,DEPT_USE_YN)\r\n"
			+ "    VALUES(SEQ_DEPT_CODE.NEXTVAL,:name,:useYn)", nativeQuery = true)
	int insertQuery(String name, String useYn);

	Department findDepartmentByName(String deptName);
}
