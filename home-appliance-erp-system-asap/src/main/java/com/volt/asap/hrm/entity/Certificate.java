package com.volt.asap.hrm.entity;

import javax.persistence.*;

@Entity(name = "Certificate")
@Table(name = "TBL_CERTIFICATE")
@SequenceGenerator(
        name = "CERTIFICATE_CODE_SEQ_GENERATOR",
        sequenceName = "SEQ_CERTIFICATE_CODE",
        initialValue = 47,
        allocationSize = 1
)
public class Certificate {

	@Id
	@GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "CERTIFICATE_CODE_SEQ_GENERATOR"
    )
	@Column(name = "CERTIFICATE_CODE")
	private int certificateCode;
	
	@Column(name = "EMP_NAME")
	private String empName;
	
	@Column(name = "PURPOSE")
	private String purpose;
	
	@Column(name = "PUBLISHED_DATE")
	private String publishedDate;
	
	@Column(name = "EMP_CODE")
	private int empCode;
	
	@Column(name = "CERTIFICATE_TYPE_CODE")
	private String certificateTypeCode;

	public Certificate() {
	}

	public Certificate(int certificateCode, String empName, String purpose, String publishedDate, int empCode,
			String certificateTypeCode) {
		this.certificateCode = certificateCode;
		this.empName = empName;
		this.purpose = purpose;
		this.publishedDate = publishedDate;
		this.empCode = empCode;
		this.certificateTypeCode = certificateTypeCode;
	}

	public int getCertificateCode() {
		return certificateCode;
	}

	public void setCertificateCode(int certificateCode) {
		this.certificateCode = certificateCode;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public String getCertificateTypeCode() {
		return certificateTypeCode;
	}

	public void setCertificateTypeCode(String certificateTypeCode) {
		this.certificateTypeCode = certificateTypeCode;
	}

	@Override
	public String toString() {
		return "Certificate [certificateCode=" + certificateCode + ", empName=" + empName + ", purpose=" + purpose
				+ ", publishedDate=" + publishedDate + ", empCode=" + empCode + ", certificateTypeCode="
				+ certificateTypeCode + "]";
	}
}
