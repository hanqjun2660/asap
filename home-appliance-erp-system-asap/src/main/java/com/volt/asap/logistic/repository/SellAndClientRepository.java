package com.volt.asap.logistic.repository;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.logistic.entity.SellAndClient;

public interface SellAndClientRepository extends JpaRepository<SellAndClient, String> {

	List<SellAndClient> findByProductProductNameContaining(String searchValue1);

	List<SellAndClient> findByClientNameContaining(String searchValue2, Pageable paging);

	int countByProductProductNameContaining(String searchValue1);

	int countByClientNameContaining(String searchValue2);

	List<SellAndClient> findByProductProductNameContaining(String searchValue1, Pageable paging);

	SellAndClient findSellAndClientEntityBySellNumber(int sellNumber);

	int countBySellDateBetween(Date startDate, Date endDate);

	int countBySellDateBetweenAndProductProductNameContaining(Date startDate, Date endDate, String searchProduct);


	int countBySellDateBetweenAndClientNameContaining(Date startDate, Date endDate, String searchClient);

	List<SellAndClient> findAllBySellDateBetween(Date startDate, Date endDate, Pageable paging);

	List<SellAndClient> findAllBySellDateBetweenAndProductProductName(Date startDate, Date endDate, String searchProduct, Pageable paging);

	List<SellAndClient> findAllBySellDateBetweenAndClientName(Date startDate, Date endDate, String searchClient,
			Pageable paging);

	List<SellAndClient> findAllByProductProductName(String searchProduct,
			Pageable paging);

	List<SellAndClient> findAllByClientName(String searchClient, Pageable paging);

	List<SellAndClient> findAllBySellDateBetweenAndClientNameAndProductProductName(Date startDate, Date endDate,
			String searchClient, String searchProduct, Pageable paging);

	List<SellAndClient> findAllBySellDateBetweenAndProductProductNameContaining(Date startDate, Date endDate,
			String searchProduct, Pageable paging);

	List<SellAndClient> findAllBySellDateBetweenAndClientNameContaining(Date startDate, Date endDate,
			String searchClient, Pageable paging);

	List<SellAndClient> findAllBySellDateBetweenAndClientNameAndProductProductNameContaining(Date startDate,
			Date endDate, String searchClient, String searchProduct, Pageable paging);

	List<SellAndClient> findAllByProductProductNameContaining(String searchProduct, Pageable paging);

	List<SellAndClient> findAllByClientNameContaining(String searchClient, Pageable paging);

	List<SellAndClient> findAllByProductProductNameContainingAndClientNameContaining(String searchProduct, String searchClient,
			Pageable paging);

}
