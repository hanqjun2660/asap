package com.volt.asap.logistic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "StockHistoryAndProductAndStorage")
@Table(name = "TBL_STOCK_HISTORY")
@SequenceGenerator(
		name = "STOCK_HISTORY_SEQ_GENERATOR",
		sequenceName = "SEQ_STOCK_HISTORY_NO",
		initialValue = 31,
		allocationSize = 1		
		)
public class StockHistoryAndProductAndStorage {

	@Id
	@Column(name = "STOCK_NO")
	private int stockNo;
	
	@Column(name = "STOCK_QUANTITY")
	private int stockQuantity;
	
	@ManyToOne
	@JoinColumn(name = "PRODUCT_CODE")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "STORAGE_NO")
	private Storage storage;

	public StockHistoryAndProductAndStorage() {
	}

	public StockHistoryAndProductAndStorage(int stockNo, int stockQuantity, Product product, Storage storage) {
		this.stockNo = stockNo;
		this.stockQuantity = stockQuantity;
		this.product = product;
		this.storage = storage;
	}

	public int getStockNo() {
		return stockNo;
	}

	public void setStockNo(int stockNo) {
		this.stockNo = stockNo;
	}

	public int getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	@Override
	public String toString() {
		return "StockHistoryAndProductAndStorage [stockNo=" + stockNo + ", stockQuantity=" + stockQuantity
				+ ", product=" + product + ", storage=" + storage + "]";
	}

}
