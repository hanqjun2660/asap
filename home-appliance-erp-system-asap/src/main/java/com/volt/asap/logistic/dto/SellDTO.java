package com.volt.asap.logistic.dto;

import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.volt.asap.sales.dto.ClientDTO;

public class SellDTO {
	
	private int sellNumber;
	private java.sql.Date sellDate;
	private int sellAmount;
	private long sellTotalPrice;
	private String productCode;
	private int clientNumber;
	private int storageNo;
	private long sellPrice;
	
	private ClientDTO client;
	private ProductDTO product;
	private StorageDTO storage;
	
	
	public SellDTO() {
	}


	public SellDTO(int sellNumber, Date sellDate, int sellAmount, long sellTotalPrice, String productCode,
			int clientNumber, int storageNo, long sellPrice, ClientDTO client, ProductDTO product, StorageDTO storage) {
		super();
		this.sellNumber = sellNumber;
		this.sellDate = sellDate;
		this.sellAmount = sellAmount;
		this.sellTotalPrice = sellTotalPrice;
		this.productCode = productCode;
		this.clientNumber = clientNumber;
		this.storageNo = storageNo;
		this.sellPrice = sellPrice;
		this.client = client;
		this.product = product;
		this.storage = storage;
	}


	public int getSellNumber() {
		return sellNumber;
	}


	public void setSellNumber(int sellNumber) {
		this.sellNumber = sellNumber;
	}


	public java.sql.Date getSellDate() {
		return sellDate;
	}


	public void setSellDate(java.sql.Date sellDate) {
		this.sellDate = sellDate;
	}


	public int getSellAmount() {
		return sellAmount;
	}


	public void setSellAmount(int sellAmount) {
		this.sellAmount = sellAmount;
	}


	public long getSellTotalPrice() {
		return sellTotalPrice;
	}


	public void setSellTotalPrice(long sellTotalPrice) {
		this.sellTotalPrice = sellTotalPrice;
	}


	public String getProductCode() {
		return productCode;
	}


	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	public int getClientNumber() {
		return clientNumber;
	}


	public void setClientNumber(int clientNumber) {
		this.clientNumber = clientNumber;
	}


	public int getStorageNo() {
		return storageNo;
	}


	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}


	public long getSellPrice() {
		return sellPrice;
	}


	public void setSellPrice(long sellPrice) {
		this.sellPrice = sellPrice;
	}


	public ClientDTO getClient() {
		return client;
	}


	public void setClient(ClientDTO client) {
		this.client = client;
	}


	public ProductDTO getProduct() {
		return product;
	}


	public void setProduct(ProductDTO product) {
		this.product = product;
	}


	public StorageDTO getStorage() {
		return storage;
	}


	public void setStorage(StorageDTO storage) {
		this.storage = storage;
	}


	@Override
	public String toString() {
		return "SellDTO [sellNumber=" + sellNumber + ", sellDate=" + sellDate + ", sellAmount=" + sellAmount
				+ ", sellTotalPrice=" + sellTotalPrice + ", productCode=" + productCode + ", clientNumber="
				+ clientNumber + ", storageNo=" + storageNo + ", sellPrice=" + sellPrice + ", client=" + client
				+ ", product=" + product + ", storage=" + storage + ", getSellNumber()=" + getSellNumber()
				+ ", getSellDate()=" + getSellDate() + ", getSellAmount()=" + getSellAmount() + ", getSellTotalPrice()="
				+ getSellTotalPrice() + ", getProductCode()=" + getProductCode() + ", getClientNumber()="
				+ getClientNumber() + ", getStorageNo()=" + getStorageNo() + ", getSellPrice()=" + getSellPrice()
				+ ", getClient()=" + getClient() + ", getProduct()=" + getProduct() + ", getStorage()=" + getStorage()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	
	
}
