package com.volt.asap.logistic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.logistic.dto.InventoryDTO;
import com.volt.asap.logistic.entity.JoinInventory;

public interface JoinInventoryRepository extends JpaRepository<JoinInventory, Integer>{

	List<JoinInventory> findJoinInventoryByStockHistoryStockNo(int stockNo);
//	List<JoinInventory> findJoinInventoryByStockNo(int stockNo);

}
