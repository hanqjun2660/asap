package com.volt.asap.logistic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "StockHistory")
@Table(name = "TBL_STOCK_HISTORY")
@SequenceGenerator(
		name = "STOCK_HISTORY_SEQ_GENERATOR",
		sequenceName = "SEQ_STOCK_HISTORY_NO",
		initialValue = 31,
		allocationSize = 1		
		)
public class StockHistory {
	@Id
	@Column(name = "STOCK_NO")
	private int stockNo;
	
	@Column(name = "STOCK_QUANTITY")
	private int stockQuantity;
	
	@Column(name = "PRODUCT_CODE")
	private String productCode;

	@Column(name = "STORAGE_NO")
	private int storageNo;
	
//	@ManyToOne
//	@JoinColumn(name = "PRODUCT_CODE")
//	private Product product;
	
//	@ManyToOne
//	@JoinColumn(name = "STORAGE_NO")
//	private Storage storage;
	
	public StockHistory() {
	}

	public StockHistory(int stockNo, int stockQuantity, String productCode, int storageNo) {
		this.stockNo = stockNo;
		this.stockQuantity = stockQuantity;
		this.productCode = productCode;
		this.storageNo = storageNo;
	}

	public int getStockNo() {
		return stockNo;
	}

	public void setStockNo(int stockNo) {
		this.stockNo = stockNo;
	}

	public int getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}

	@Override
	public String toString() {
		return "StockHistory [stockNo=" + stockNo + ", stockQuantity=" + stockQuantity + ", productCode=" + productCode
				+ ", storageNo=" + storageNo + "]";
	}

}
