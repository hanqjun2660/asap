package com.volt.asap.logistic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "ProductAndStoAndProCtg")
@Table(name = "TBL_PRODUCT")
public class ProductAndStoAndProCtg {

	@Id
	@Column(name = "PRODUCT_CODE")
	private String productCode;
	
	@Column(name = "PRODUCT_NAME")
	private String productName;
	
	@Column(name = "PRODUCT_RELEASE")
	private int productRelease;
	
	@Column(name = "PRODUCT_WAEARING")
	private int productWaearing;
	
	@ManyToOne
	@JoinColumn(name = "STORAGE_NO")
	private Storage storage;
	
	@ManyToOne
	@JoinColumn(name = "CATEGORY_CODE")
	private Category category;
	
	@Column(name = "PRODUCT_STATUS")
	private String productStatus;

	public ProductAndStoAndProCtg() {
	}

	public ProductAndStoAndProCtg(String productCode, String productName, int productRelease, int productWaearing,
			Storage storage, Category category, String productStatus) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.productRelease = productRelease;
		this.productWaearing = productWaearing;
		this.storage = storage;
		this.category = category;
		this.productStatus = productStatus;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductRelease() {
		return productRelease;
	}

	public void setProductRelease(int productRelease) {
		this.productRelease = productRelease;
	}

	public int getProductWaearing() {
		return productWaearing;
	}

	public void setProductWaearing(int productWaearing) {
		this.productWaearing = productWaearing;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	@Override
	public String toString() {
		return "ProductAndStoAndProCtg [productCode=" + productCode + ", productName=" + productName
				+ ", productRelease=" + productRelease + ", productWaearing=" + productWaearing + ", storage=" + storage
				+ ", category=" + category + ", productStatus=" + productStatus + "]";
	}

	
}
