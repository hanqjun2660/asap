package com.volt.asap.logistic.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name="Sell")
@Table(name="TBL_SELL_LIST")
@SequenceGenerator(
		name = "SELL_SEQ_GENERATOR",
		sequenceName = "SEQ_SELL_NO",
		initialValue = 13,
		allocationSize = 1		
		)
public class Sell {
	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "SELL_SEQ_GENERATOR"
	)
	@Column(name = "SELL_NO")
	private int sellNumber;
	
	@Column(name = "SELL_DATE")
	private java.sql.Date sellDate;
	
	@Column(name = "SELL_AMOUNT")
	private int sellAmount;
	
	@Column(name = "SELL_TOTAL_PRICE")
	private long sellTotalPrice;
	
	@Column(name = "PRODUCT_CODE")
	private String productCode;
	
	@Column(name = "CLIENT_NO")
	private int clientNumber;
	
	@Column(name = "STORAGE_NO")
	private long storageNo;
	
	@Column(name = "SELL_PRICE")
	private long sellPrice;
	
	public Sell() {
	}

	public Sell(int sellNumber, Date sellDate, int sellAmount, long sellTotalPrice, String productCode,
			int clientNumber, long storageNo, long sellPrice) {
		super();
		this.sellNumber = sellNumber;
		this.sellDate = sellDate;
		this.sellAmount = sellAmount;
		this.sellTotalPrice = sellTotalPrice;
		this.productCode = productCode;
		this.clientNumber = clientNumber;
		this.storageNo = storageNo;
		this.sellPrice = sellPrice;
	}

	public int getSellNumber() {
		return sellNumber;
	}

	public void setSellNumber(int sellNumber) {
		this.sellNumber = sellNumber;
	}

	public java.sql.Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(java.sql.Date sellDate) {
		this.sellDate = sellDate;
	}

	public int getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(int sellAmount) {
		this.sellAmount = sellAmount;
	}

	public long getSellTotalPrice() {
		return sellTotalPrice;
	}

	public void setSellTotalPrice(long sellTotalPrice) {
		this.sellTotalPrice = sellTotalPrice;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getClientNumber() {
		return clientNumber;
	}

	public void setClientNumber(int clientNumber) {
		this.clientNumber = clientNumber;
	}

	public long getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(long storageNo) {
		this.storageNo = storageNo;
	}

	public long getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(long sellPrice) {
		this.sellPrice = sellPrice;
	}

	@Override
	public String toString() {
		return "Sell [sellNumber=" + sellNumber + ", sellDate=" + sellDate + ", sellAmount=" + sellAmount
				+ ", sellTotalPrice=" + sellTotalPrice + ", productCode=" + productCode + ", clientNumber="
				+ clientNumber + ", storageNo=" + storageNo + ", sellPrice=" + sellPrice + "]";
	}
	
	
	
}
