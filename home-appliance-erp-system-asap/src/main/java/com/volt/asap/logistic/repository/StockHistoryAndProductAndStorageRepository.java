package com.volt.asap.logistic.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.logistic.entity.StockHistoryAndProductAndStorage;

public interface StockHistoryAndProductAndStorageRepository extends JpaRepository<StockHistoryAndProductAndStorage, Integer> {

	List<StockHistoryAndProductAndStorage> findByStorageStorageNoAndProductProductCode(int searchCondition, String searchValue, Pageable paging);

	List<StockHistoryAndProductAndStorage> findByStorageStorageName(String storage);

}
