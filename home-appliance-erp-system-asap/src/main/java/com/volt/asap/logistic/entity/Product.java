package com.volt.asap.logistic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Product")
@Table(name = "TBL_PRODUCT")
public class Product {

   @Id
   @Column(name = "PRODUCT_CODE")
   private String productCode;
   
   @Column(name = "PRODUCT_NAME")
   private String productName;
   
   @Column(name = "PRODUCT_RELEASE")
   private int productRelease;
   
   @Column(name = "PRODUCT_WAEARING")
   private int productWaearing;
   
   @Column(name = "STORAGE_NO")
   private int storageNo;
   
   @Column(name = "CATEGORY_CODE")
   private String categoryCode;
   
   @Column(name = "PRODUCT_STATUS")
   private String productStatus;

   public Product() {
   }

   public Product(String productCode, String productName, int productRelease, int productWaearing, int storageNo,
         String categoryCode, String productStatus) {
      super();
      this.productCode = productCode;
      this.productName = productName;
      this.productRelease = productRelease;
      this.productWaearing = productWaearing;
      this.storageNo = storageNo;
      this.categoryCode = categoryCode;
      this.productStatus = productStatus;
   }

   public String getProductCode() {
      return productCode;
   }

   public void setProductCode(String productCode) {
      this.productCode = productCode;
   }

   public String getProductName() {
      return productName;
   }

   public void setProductName(String productName) {
      this.productName = productName;
   }

   public int getProductRelease() {
      return productRelease;
   }

   public void setProductRelease(int productRelease) {
      this.productRelease = productRelease;
   }

   public int getProductWaearing() {
      return productWaearing;
   }

   public void setProductWaearing(int productWaearing) {
      this.productWaearing = productWaearing;
   }

   public int getStorageNo() {
      return storageNo;
   }

   public void setStorageNo(int storageNo) {
      this.storageNo = storageNo;
   }

   public String getCategoryCode() {
      return categoryCode;
   }

   public void setCategoryCode(String categoryCode) {
      this.categoryCode = categoryCode;
   }

   public String getProductStatus() {
      return productStatus;
   }

   public void setProductStatus(String productStatus) {
      this.productStatus = productStatus;
   }

   @Override
   public String toString() {
      return "Product [productCode=" + productCode + ", productName=" + productName + ", productRelease="
            + productRelease + ", productWaearing=" + productWaearing + ", storageNo=" + storageNo
            + ", categoryCode=" + categoryCode + ", productStatus=" + productStatus + "]";
   }

   
}