package com.volt.asap.logistic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.logistic.entity.StockHistory;

public interface StockHistoryRepository extends JpaRepository<StockHistory, String> {

	int countByStorageNoAndProductCode(int searchCondition, String searchValue);

}
